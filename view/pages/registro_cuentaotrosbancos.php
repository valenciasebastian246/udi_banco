<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="index.php?co=guardarCuentaOtrosBancos" method="POST">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <h6>Datos de del Banco Beneficiario</h6>
                            <small><span>Llenado de datos</span></small>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Datos del Banco del Beneficiario
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_idbanco" class="col-md-4 col-form-label text-right">
                                                <small><strong>Banco:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <select name="txt_idbanco" id="txt_idbanco"
                                                    class="form-control bg-primary text-light">
                                                    <option >Selecciona un tipo de identificación</option>
                                                    <?php  foreach ($this->model->ListarBancosOtros() as $lista):?>
                                                    <option value="<?php echo $lista->id; ?>">
                                                    
                                                        <?php echo $lista->nombre; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_idsucursal" class=" col-md-4 col-form-label text-right">
                                                <small><strong>Sucursal:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <select name="txt_idsucursal" id="txt_idsucursal"
                                                    class="form-control bg-primary text-light">
                                                    <option selected>Selecciona una sucursal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_tipo_cuenta" class="col-md-4 col-form-label text-right">
                                                <small><strong>Tipo de Cuenta:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <select id="txt_tipo_cuenta" name="txt_tipo_cuenta"
                                                    class="form-control  custom-select bg-primary text-light">
                                                    <option selected>Selecciona una tipo de cuenta</option>
                                                    <?php  foreach ($this->model->ListarTipoCuentas() as $lista):?>
                                                    <option value="<?php echo $lista->id; ?>">
                                                        <?php echo $lista->descripcion; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_fecha_vencimiento"
                                                class="col-md-4 col-form-label text-right">
                                                <small><strong>Fecha de Vencimiento:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="date" class="form-control" name="txt_fecha_vencimiento">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_num_cuenta" class="col-md-4 col-form-label text-right">
                                                <small><strong>Número de Cuenta a Adicionar:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="txt_num_cuenta"
                                                    name="txt_num_cuenta" value="" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_moneda" class="col-md-4 col-form-label text-right">
                                                <small><strong>Moneda:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <select id="txt_moneda" name="txt_moneda"
                                                    class="form-control  custom-select bg-primary text-light">
                                                    <option selected>Seleccione una moneda</option>
                                                    <?php  foreach ($this->model->ListarTipoMoneda() as $lista):?>
                                                    <option value="<?php echo $lista->id; ?>">
                                                        <?php echo $lista->descripcion; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_titular" class="col-md-2 col-form-label text-right">
                                                <small><strong>Titular:</strong></small>
                                            </label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" id="txt_titular"
                                                    name="txt_titular" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_tipo_identificacion"
                                                class=" col-md-4 col-form-label text-right">
                                                <small><strong>Tipo de Identificación:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <select name="txt_tipo_identificacion" id="txt_tipo_identificacion"
                                                    class="form-control bg-primary text-light">
                                                    <option selected>Selecciona un tipo de identificación</option>
                                                    <?php  foreach ($this->model->ListarTipoIdentificacion() as $lista):?>
                                                    <option value="<?php echo $lista->id; ?>">
                                                        <?php echo $lista->nombre; ?> </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_numero_identificacion"
                                                class="col-md-4 col-form-label text-right">
                                                <small><strong>Número de Identificación:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="txt_numero_identificacion"
                                                    name="txt_numero_identificacion" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_direccion" class="col-md-4 col-form-label text-right">
                                                <small><strong>Dirección:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="txt_direccion"
                                                    name="txt_direccion" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <label for="txt_correo" class="col-md-4 col-form-label text-right">
                                                <small><strong>Correo Electrónico:</strong></small>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="email" class="form-control" id="txt_correo"
                                                    name="txt_correo" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" readonly id="txt_titular_cuenta" name="txt_titular_cuenta" value="">

                        <div class="card-footer bg-white">
                            <div class="form-row">
                                <div class="col text-left">
                                    <a href="index.php" class="btn btn-primary">
                                        <i class="fas fa-home"></i> Inicio
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        Continuar <i class="fas fa-caret-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var idCliente = localStorage.getItem('usuario_id');
        $('#txt_titular_cuenta').val(idCliente);

        $('#txt_idbanco').change(function () {
            var x = $('#txt_idbanco').val();
            //alert(x);
            recargarSucursales();

        });

    })
</script>
<!--AJAX-->
<script type="text/javascript">
    function recargarSucursales() {
        var idBanco = $('#txt_idbanco').val();
        $.ajax({
            type: "POST",
            url: "view/ajax/listarSucursalesPorBanco.php",
            data: {
                idBanco
            },
            success: function (r) {
                console.log(r);
                $('#txt_idsucursal').html(r);
            },
            error: function (h) {
                alert(h + "mal");
            }

        });
    }
</script>