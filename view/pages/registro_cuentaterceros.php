<section class="main">
    <div class="container">
        <div class="row">
       
            <div class="col">
                <form action="index.php?c=guardarCuentaTerceros" method="POST">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <h6>Registro de Cuentas Bancaria de Terceros</h6>
                            <small><span>Llenado de datos</span></small>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                        <input type="hidden" class="form-control" readonly id="txt_titular_cuenta_debito" name="txt_titular_cuenta_debito">
                                            <div class="form-group row">
                                                <label for="txt_num_cuenta_tercero" class="col-md-4 col-form-label text-right">Nro Cuenta :</label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control" id="txt_num_cuenta" required name="txt_num_cuenta">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_fecha_vencimiento"  class="col-md-4 col-form-label text-right">Fecha de Vencimiento :</label>
                                                <div class="col-md-8">
                                                    <input type="date" class="form-control"  name="txt_fecha_vencimiento">
                                                </div>
                                            </div>
                                         

                                    <div class="container" id="idContenido" > 
                                    
                                         <div class="form-group row" style="display:none;">
                                                    <label for="headline" class="col-md-4 col-form-label text-right">Nombre Completo :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" readonly id="txtidCliente" name="txtidCliente">

                                                    </div>
                                                </div>    
                                            
                                     </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="form-row">
                                <div class="col text-left">
                                    <a href="index.php" class="btn btn-primary">
                                        <i class="fas fa-home"></i> Inicio
                                    </a>
                                    <button type="submit" id="btn_registrar" style="display: none;" class="btn btn-primary">
                                        continuar <i class="fas fa-caret-right"></i>
                                    </button>

                                    <button type="button"  id="btn_consultar" class="btn btn-primary">
                                        consultar <i class="fas fa-caret-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

	$(document).ready(function(){
        var usuario_id= localStorage.getItem('usuario_id');
        $('#txt_titular_cuenta_debito').val(usuario_id);
    $('#btn_consultar').click(function(){
    
       var x= $('#txt_num_cuenta').val();
			verificarCuenta();
            //alert(x);
        });
    
        $('#txt_num_cuenta').change(function(){
            $("#btn_registrar").hide();
            $("#btn_consultar").show();

     });
          

	})



    function verificarCuenta(){

        var num_cuenta= $("#txt_num_cuenta").val()
        $.ajax({
            type:"POST",
            url:"view/ajax/verificarCuentaterceros.php",
            data:{num_cuenta},
            success:function(r){
               $("#btn_registrar").show();
               $("#btn_consultar").hide();
                $('#idContenido').html(r);
               //alert(r+ "BIEEN");
            }, error:function(h){
                $("#btn_registrar").hide();
               $("#btn_consultar").show();
                alert(h+"mal");
            }

        });
	
    }
</script>

