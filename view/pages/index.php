
<br>
<section >
  <div class="container-fluid"  >
    <div class="jumbotron jumbotron-fluid" style="padding-left: 50px; background-color: #eeeeee;">
    <div class="row">
        <div class="col-md-6 col-sm-12">
          <h1 class="display-4 animated zoomIn">BANCO DE SANTA CRUZ</h1>
          <p class="lead">Bienvenidos</p>
          <button id="idCambiar_usuario" class="btn btn-primary" type="button"> Cambiar usuario</button>
          <a class="btn btn-primary" href="index.php?cl=registrarCliente" type="button">Registrar Cliente</a>
              </div>
                  <div class="col-md-6 col-sm-12" style="padding-right: 50px;">          
               
                          <?php include 'Detalle_por_cliente.php' ?>

                </div>
      </div>
    </div>

  </div>
 
</section>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-danger" id="exampleModalLabel">Selecciona un cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="miTabla" class="table table-bordered table-hover">
            <thead class="thead-light">
                  <tr>
                        <th scope="col">#</th>
                        <th scope="col">nombre completo</th>
                    
          
                  </tr>
            </thead>
              <tbody id="tbody" style="cursor: pointer;"> 
                 
                    <?php foreach ($this->model->listarClientes() as $listaC): ?>
                      <tr>
                        <td ><?php echo $listaC['idCliente']; ?></td>
                        <td ><?php echo $listaC['nombre_completo']; ?></td>
            
                     </tr> 
                  <?php endforeach; ?>

              </tbody>      

        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
     
      </div>
    </div>
  </div>
</div>


<!--LISTA LAS ULTIMAS TRANSACCIONES REALIZADAS EN EL DIA -->

<?php  if($this->model->listarUltimasTransacciones()!=null) {
  ?>
<table id="dtDynamicVerticalScrollExample"  class="table table-striped table-bordered table-sm" cellspacing="0">
    <thead class="thead-dark">
    <center><h3> ULTIMAS TRANSACCIONES REALIZADAS (GENERAL)</h3></center>
            <tr>
                    <th scope="col">Cliente</th>
                   <th scope="col">Cuenta origen</th>
                   <th scope="col">monto deposito</th>
                   <th scope="col">fecha</th>
                   <th scope="col">cuenta destino</th>
                   <th scope="col">moneda</th>
                   <th scope="col">glosa</th>
                   

            </tr>
    </thead>
         <tbody>
            
        
           <?php  foreach ($this->model->listarUltimasTransacciones() as $lista):?>
            <tr>
                   <td><?php echo $lista->nombreCliente; ?></td>
                   <td><?php echo $lista->origenCuenta; ?></td>
                   <td><?php echo $lista->monto_deposito; ?></td>
                   <td><?php echo $lista->fecha; ?></td>
                   <td><?php echo $lista->CuentaDeposito; ?></td>
                   <td><?php echo $lista->tipoMoneda; ?></td>
                   <td><?php echo $lista->glosa; ?></td>
                   </tr>                           
          <?php endforeach; ?>
         </tbody>      


</table>
           <?php  } ?>

    

 <script type="text/javascript">

        $(document).ready(function(){
      
            var usuario_nombre= localStorage.getItem('usuario_nombre');  
           // alert(usuario_nombre);
           //localStorage.removeItem("usuario_nombre");
           recargarCuentasPersonales();
           recargarCuentasTerceros(); 
           recargarCuentasOtros();
            if(usuario_nombre!=null){
              $('.lead').text("Bienvenido"+ " "+usuario_nombre);
          
              $('#myModal').modal('hide')    
            }else{
              $('#myModal').modal('show'); 
            }
            ver_id();
            $('#idCambiar_usuario').click(function(){
              $('#myModal').modal('show'); 
            });
   
        }); 
      function ver_id() {
        if (!document.getElementsByTagName || !document.createTextNode) return;
          var rows = document.getElementById('miTabla').getElementsByTagName('tr');
            for (i = 0; i < rows.length; i++) {
              rows[i].onclick = function() {
                  var id_cliente = this.getElementsByTagName('td')[0].innerHTML;
                var nombre = this.getElementsByTagName('td')[1].innerHTML;
                localStorage.setItem('usuario_id',id_cliente);
                localStorage.setItem('usuario_nombre',nombre);
                alert(id_cliente+" "+nombre)
                  $('#myModal').modal('hide'); 
                  $('.lead').text("Bienvenido"+ " "+nombre);  
                  $('#nombre_cliente').text(" "+nombre);  
                  recargarCuentasPersonales();
                  recargarCuentasTerceros();
                  recargarCuentasOtros();
              }
          }
      }


      function recargarCuentasPersonales(){
        var cuentaP=localStorage.getItem('usuario_id');
        $("#tbody_datos").hide();
       // alert(cuentaP);
        $.ajax({
            type:"POST",
            url:"view/ajax/listarCuentaPersonalPorCliente.php",
            data:{cuentaP},
            success:function(r){
                console.log(r);
                $('#select_cliente').html(r);
                //alert(r+"bien");
            }, error:function(h){
                alert(h+"mal");
            }

          });    
      }

      function recargarCuentasTerceros(){
        var cuentaT=localStorage.getItem('usuario_id');
        $("#tbody_datos").hide();
        $.ajax({
            type:"POST",
            url:"view/ajax/listarCuentaTerceroPorCliente.php",
            data:{cuentaT},
            success:function(r){
                console.log(r);
                $('#select_cliente_t').html(r);
                //alert(r+"bien");
            }, error:function(h){
                alert(h+"mal");
            }

          });    
      }

      function recargarCuentasOtros(){
        var cuentaO=localStorage.getItem('usuario_id');
        $("#tbody_datos").hide();
        $.ajax({
            type:"POST",
            url:"view/ajax/listarCuentaOtrosPorCliente.php",
            data:{cuentaO},
            success:function(r){
                console.log(r);
                $('#select_cliente_o').html(r);
                //alert(r+"bien");
            }, error:function(h){
                alert(h+"mal");
            }

          });    
      }

    

</script>