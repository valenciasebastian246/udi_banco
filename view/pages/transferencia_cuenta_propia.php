<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="index.php?cp=guardarTransaccionCuentaPropias" method="POST">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <h6>Transferencias entre cuentras propias</h6>
                            <small><span>Llenado de datos</span></small>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="txt_cuenta_origen" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Cuenta Origen:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="txt_cuenta_origen" name="txt_cuenta_origen" class="form-control  custom-select bg-primary text-light" required>
                                                        <option selected>Selecciona una cuenta origen</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_cuenta_destino" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Cuenta Destino:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="txt_cuenta_destino" name="txt_cuenta_destino" class="form-control  custom-select bg-primary text-light" required>
                                                        <option selected>Seleccione una cuenta destino</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_monto_transferir" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Monto a Transferir:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="number" min="1" step=".01"  class="form-control" id="txt_monto_transferir" name="txt_monto_transferir" value="" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_tipo_moneda" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Moneda:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="txt_tipo_moneda" name="txt_tipo_moneda" class="form-control  custom-select bg-primary text-light" required>
                                                        <option selected>Seleccione una Moneda</option>
                                                        <?php foreach ($this->model->listarTipoMoneda() as $lista) : ?>
                                                            <option value="<?php echo $lista->idTipoMoneda; ?>"><?php echo $lista->descripcion; ?> </option>

                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_glosa" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Glosa:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="txt_glosa" name="txt_glosa" value="" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_origen_fondo_descrip" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Origen de Fondos:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="txt_origen_fondo_descrip" name="txt_origen_fondo_descrip" value="" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_destino_fondo_descrip" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Destino de Fondos:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="txt_destino_fondo_descrip" name="txt_destino_fondo_descrip" value="" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" readonly id="txt_titular_cuenta" name="txt_titular_cuenta" value="">
                        
                        <div class="card-footer bg-white">
                            <div class="form-row">
                                <div class="col text-left">
                                    <a href="index.php" class="btn btn-primary">
                                        <i class="fas fa-home"></i> Inicio
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        Continuar <i class="fas fa-caret-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var usuario_nombre = localStorage.getItem('usuario_nombre');
        var cuentaP = localStorage.getItem('usuario_id');
        $('#txt_titular_cuenta').val(cuentaP);

        CuentasPersonales();
        
        $('#txt_cuenta_origen').change(function () {
            var x = $('#txt_idbanco').val();
            //alert(x);
            recargarCuentasPersonalesParaTransferir();

        });
    });

    function CuentasPersonales() {
        var cuentaP = localStorage.getItem('usuario_id');
        //alert(cuentaP);
        $.ajax({
            type: "POST",
            url: "view/ajax/listarCuentaPersonalPorCliente.php",
            data: {
                cuentaP
            },
            success: function(r) {
                console.log(r);

                $('#txt_cuenta_origen').html(r);
            },
            error: function(h) {
                alert(h + "mal");
            }

        });
    }
    function recargarCuentasPersonalesParaTransferir() {
        var idCliente = localStorage.getItem('usuario_id');
        var cuentaPersonalOrigen = $('#txt_cuenta_origen').val();
        $.ajax({
            type: "POST",
            url: "view/ajax/listarCuentaPersonalPorClienteParaTransferir.php",
            data: {
                idCliente,
                cuentaPersonalOrigen
            },
            success: function(r) {
                console.log(r);

                $('#txt_cuenta_destino').html(r);
            },
            error: function(h) {
                alert(h + "mal");
            }

        });
    }
</script>