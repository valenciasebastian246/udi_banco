
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="index.php?c=guardarTransaccionCuentaTerceros" method="POST">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <h6>Transferir a Cuentas de Terceros</h6>
                            <small><span>Seleccione la cuenta origen y el destinatario</span></small>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    Datos de Cuenta 
                                
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                    <div  class="col-md-6 col-sm-12" style="display: none;"> 
                                            <div class="form-group row">
                                                <label for="txt_titular_cuenta"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Titular Cuenta:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                <input type="text" class="form-control" readonly id="txt_titular_cuenta_debito" name="txt_titular_cuenta_debito">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-9 col-sm-12">
                                            <div class="form-group row">
                                                <label for="txt_cuenta_origen"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Cuenta Origen:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="txt_cuenta_origen" name="txt_cuenta_origen" required
                                                        class="form-control custom-select bg-primary text-light">
                                                        <option>Selecciona la Cuenta de Débito</option>
                                                      
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    Datos del Destinatario
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                      
                                        <div class="col-md-6 col-sm-12" >
                                            <div class="form-group row" >
                                                <label for="txt_cuenta_destino"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Cuenta Destino</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="txt_cuenta_destino" name="txt_cuenta_destino" class="form-control">
                                                        <option>seleccione una cuenta de destino</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group row">
                                                <label for="txt_titular_cuenta"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Titular Cuenta:</strong></small>
                                                </label>
                                                <div class="col-md-8"> 
                                                   
                                                        <input type="text" class="form-control" readonly id="txt_titular_cuenta" name="txt_titular_cuenta">
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    Datos Adicionales
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group row">
                                                <label for="txt_monto_transferir"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Monto a Transferir:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="number" min="1"  class="form-control" id="txt_monto_transferir" required
                                                        name="txt_monto_transferir" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_glosa" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Glosa:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="txt_glosa" required
                                                        name="txt_glosa" value="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_destino_fondo_descrip"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Destino de Fondos:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" required
                                                        id="txt_destino_fondo_descrip" name="txt_destino_fondo_descrip"
                                                        value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group row">
                                                <label for="txt_tipo_moneda" class="col-md-4 col-form-label text-right">
                                                    <small><strong>Moneda:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <select id="txt_tipo_moneda" name="txt_tipo_moneda" required
                                                        class="form-control custom-select bg-primary text-light">
                                                        <option selected>Seleccione la moneda</option>
                                                        <?php  foreach ($this->model->listarTipoMoneda() as $lista):?>
                                                                <option value="<?php echo $lista->idTipoMoneda; ?>"><?php echo $lista->descripcion; ?> </option>
                                                            <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_origen_fondo_descrip"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Origen de Fondos:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" required
                                                        id="txt_origen_fondo_descrip" name="txt_origen_fondo_descrip"
                                                        value="">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_mensaje_adjunto_email"
                                                    class="col-md-4 col-form-label text-right">
                                                    <small><strong>Mensaje Adjunto al Correo
                                                            Electrónico:</strong></small>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                        id="txt_mensaje_adjunto_email" name="txt_mensaje_adjunto_email"
                                                        value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="form-row">
                                <div class="col text-left">
                                    <a href="index.php" class="btn btn-primary">
                                        <i class="fas fa-home"></i> Inicio
                                    </a>
                                    <button type="submit" id="btn_continuar" class="btn btn-primary">
                                        Continuar <i class="fas fa-caret-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
        var usuario_nombre=localStorage.getItem('usuario_nombre');
       
        var usuario_id= localStorage.getItem('usuario_id');
       // $('#txt_titular_cuenta').select2();
        $('#txt_cuenta_destino').select2();
        $('#txt_cuenta_origen').select2();
       
     
        $('#txt_titular_cuenta_debito').val(usuario_id);
    $('#txt_cuenta_destino').change(function(){   
       var x= $('#txt_titular_cuenta').val();
			
            verificarCuenta();
        });
        //listarTitularExceptoClienteSeleccionado();
        recargarCuentasPersonales();
        recargarLista();
        verificarCuenta();

	})
</script>

<!--AJAX-->
<script type="text/javascript">
	function recargarLista(){
        var titular= localStorage.getItem('usuario_id');
        $.ajax({
            type:"POST",
            url:"view/ajax/listarCuentaPorCliente.php",
            data:{titular},
            success:function(r){
                //swal("Good job!", "You clicked the button!", "success");
                //Swal.fire('Good job!','You clicked the button!','success')
                $('#txt_cuenta_destino').html(r);
               //alert(r+ "BIEEN");
            }, error:function(h){
                alert(h+"mal");
            }

        });
	
    }

        function verificarCuenta(){

        var idCuenta= $("#txt_cuenta_destino").val()
        //alert(idCuenta)
        $.ajax({
            type:"POST",
            dataType:"json",
            url:"view/ajax/traerClienteCuentaTerceros.php",
            data:{idCuenta},
            success:function(r){
                //$('#txt_titular_cuenta').html(r);
                $('#txt_titular_cuenta').val(r[0].nombre_completo);
           // alert(r+ "BIEEN");
            }, error:function(h){
            
                alert(h+"mal");
            }

        });

    }

    function listarTitularExceptoClienteSeleccionado(){
        var usuario_id=localStorage.getItem('usuario_id');
        $.ajax({
            type:"POST",
            url:"view/ajax/listarClienteExceptoSeleccionado.php",
            data:{usuario_id},
            success:function(r){
                //swal("Good job!", "You clicked the button!", "success");
                //Swal.fire('Good job!','You clicked the button!','success')
                $('#txt_titular_cuenta').html(r);
               //alert(r+ "BIEEN");
            }, error:function(h){
                alert(h+"mal");
            }

        });
	
    }

    function recargarCuentasPersonales(){
        var cuentaP=localStorage.getItem('usuario_id');
       // alert(cuentaP);
        $.ajax({
            type:"POST",
            url:"view/ajax/listarCuentaPersonalPorCliente.php",
            data:{cuentaP},
            success:function(r){
                console.log(r);
            
                $('#txt_cuenta_origen').html(r);
            }, error:function(h){
                alert(h+"mal");
            }

          });
    }

</script>