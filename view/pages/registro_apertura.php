<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="index.php?cp=guardarCuentaPropia" method="POST">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <h6>Apertura de Cuenta Bancaria</h6>
                            <small><span>Llenado de datos</span></small>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="txt_cuenta_origen" class="col-md-4 col-form-label text-right">
                                                    Titular:
                                                </label>
                                                <div class="col-md-8">
                                                  <input type="text" id="cliente_nombre" class="form-control" readonly>
                                                  <input type="hidden" id="id_cliente" class="form-control" name="titular">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="cuenta" class="col-md-4 col-form-label text-right">Cuenta :</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="cuenta">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="headline" class="col-md-4 col-form-label text-right">Tasa de Intéres
                                                    Anual(%) :</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="interes">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="headline" class="col-md-4 col-form-label text-right">Fecha :</label>
                                                <div class="col-md-8">
                                                    <input type="date" class="form-control" name="fecha">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="headline" class="col-md-4 col-form-label text-right">Agencia :</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="agencia">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="headline" class="col-md-4 col-form-label text-right">Ejecutivo
                                                    :</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="ejecutivo">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer bg-white">
                            <div class="form-row">
                                <div class="col text-left">
                                    <a href="index.php" class="btn btn-primary">
                                        <i class="fas fa-home"></i> Inicio
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        Continuar <i class="fas fa-caret-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var usuario_nombre=localStorage.getItem('usuario_nombre');
        var cuentaP=localStorage.getItem('usuario_id');
        $('#cliente_nombre').val(" "+usuario_nombre);
        $('#id_cliente').val(" "+cuentaP);
    });
</script>


