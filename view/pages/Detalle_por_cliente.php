<!-- Card -->
<div class="card">

  <!--SELECT CUENTA PROPIA-->
  <div class="card-header">
      <select id="select_cliente" name="select_cliente" class="custom-select custom-select-sm">
            <option> selecciona una cuenta propia</option>
      </select> 
  </div>

  <!--SELECT CUENTA TERCERO-->
  <div class="card-header">
    <select id="select_cliente_t" name="select_cliente_t" class="custom-select custom-select-sm">
            <option> seleccione una cuenta tercero</option>
      </select>
  </div>
  
  <!--SELECT CUENTA OTRO-->
  <div class="card-header">
      <select id="select_cliente_o" name="select_cliente_o" class="custom-select custom-select-sm">
            <option> selecciona una cuenta otros</option>
      </select> 
  </div>

  <!-- Card content -->
  <div class="card-body">

    <!-- Title -->
    <h4 class="card-title text-primary">Detalles de mi cuenta</h4>
    <table class="table">
    <tbody id="tbody_datos">
      
    </tbody>
</table>

  </div>

</div>
<!-- Card -->

<script type="text/javascript">
    $(document).ready(function(){

        TraerDatos();
        $("#select_cliente").change(function(){
        var x = $("#select_cliente").val();
        //alert(x);
        TraerDatos();
        $("#tbody_datos").show();
        });

        TraerDatosCuentaTercero();
        $("#select_cliente_t").change(function(){
        var c = $("#select_cliente_t").val();
        //alert(c);
        TraerDatosCuentaTercero();
        $("#tbody_datos").show();
        });

        TraerDatosCuentaOtros();
        $("#select_cliente_o").change(function(){
        var o = $("#select_cliente_o").val();
        //alert(o);
        TraerDatosCuentaOtros();
        $("#tbody_datos").show();
        });
    });

    function TraerDatos(){
        var nro_cuenta = $("#select_cliente").val();
       // alert(nro_cuenta);
        $.ajax({
            type:"POST",
            url:"view/ajax/traer_Datos_cuenta_cliente.php",
            data:{nro_cuenta},
            success:function(r){
                console.log(r);
               // $('#select_cliente').html(r);
               $('#tbody_datos').html(r);
               //alert(r+"bien");
            }, error:function(h){
                alert(h+"mal");
            }

          });
    }

    function TraerDatosCuentaTercero(){
        var cuenta = $("#select_cliente_t").val();
        $.ajax({
            type:"POST",
            url:"view/ajax/traer_Datos_Cliente_Cuenta_Terceros.php",
            data:{cuenta},
            success:function(r){
                console.log(r);
               $('#tbody_datos').html(r);
               //alert(r+"bien");
            }, error:function(h){
                alert(h+"mal");
            }

          });
    }

    function TraerDatosCuentaOtros(){
        var cuentao = $("#select_cliente_o").val();
        $.ajax({
            type:"POST",
            url:"view/ajax/traer_Datos_Cliente_Cuenta_Otros.php",
            data:{cuentao},
            success:function(r){
                console.log(r);
               $('#tbody_datos').html(r);
               //alert(r+"bien");
            }, error:function(h){
                alert(h+"mal");
            }
          });
    } 

</script>