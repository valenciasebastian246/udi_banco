    <section class="main">
        <div class="container">
            <div class="row">
                <div class="col">
                    <form action="index.php?co=guardarTransaccionCuentaOtrosBancos" method="POST">
                        <div class="card">
                            <div class="card-header bg-primary text-white">
                                <h6>Ingreso de Datos Transferencias ACH</h6>
                                <small><span>Transferencia ACH</span></small>
                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        Datos del Banco del Beneficiario
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_idbanco" class="col-md-4 col-form-label text-right">
                                                        <small><strong>Banco:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="txt_idbanco" name="txt_idbanco"
                                                            class="form-control custom-select bg-primary text-light">
                                                            <option selected>Seleccione</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="txt_cuenta_destino"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Cuenta Destino:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="txt_cuenta_destino" name="txt_cuenta_destino"
                                                            class="form-control custom-select bg-primary text-light">
                                                            <option selected>seleccione una cuenta de destino</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_sucursal"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Sucursal:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly class="form-control" value=""
                                                            id="txt_sucursal" name="txt_sucursal">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="txt_moneda" class="col-md-4 col-form-label text-right">
                                                        <small><strong>Moneda:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly class="form-control" id="txt_moneda"
                                                            name="txt_moneda">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_tipo_cuenta"
                                                        class="col-md-4 col-form-label text-right"><small><strong>Tipo
                                                                Cuenta</strong></small></label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly class="form-control" id="txt_tipo_cuenta"
                                                            name="txt_tipo_cuenta">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        Datos del Beneficiario final
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_titular" class="col-md-2 col-form-label text-right">
                                                        <small><strong>Titular:</strong></small>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type="text" readonly class="form-control"
                                                            id="txt_titular" name="txt_titular" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_tipoidentificacion"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Tipo de Identificacion:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly class="form-control" value=""
                                                            id="txt_tipoidentificacion" name="txt_tipoidentificacion">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="txt_direccion" class="col-md-4 col-form-label text-right">
                                                        <small><strong>Dirección:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly class="form-control" id="txt_direccion"
                                                            name="txt_direccion">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_numeroidentificacion"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Numero de Identificacion:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly class="form-control" value=""
                                                            id="txt_numeroidentificacion" name="txt_numeroidentificacion">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="txt_glosa" class="col-md-4 col-form-label text-right">
                                                        <small><strong>Observaciones:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" id="txt_glosa"
                                                            name="txt_glosa" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header">
                                        Cuenta para el Debito
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_cuenta_origen"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Cuenta Origen:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="txt_cuenta_origen" name="txt_cuenta_origen" required 
                                                            class="form-control custom-select bg-primary text-light">
                                                            <option selected>Selecciona la Cuenta de Débito</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_monto_transferir"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Monto a Transferir:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="number" required step=".01" min="1" class="form-control"
                                                            id="txt_monto_transferir" name="txt_monto_transferir"
                                                            value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_tipo_moneda"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Moneda:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="txt_tipo_moneda" name="txt_tipo_moneda" required 
                                                            class="form-control custom-select bg-primary text-light">
                                                            <option selected>Seleccione la moneda</option>
                                                            <?php  foreach ($this->model->ListarTipoMoneda() as $lista):?>
                                                            <option value="<?php echo $lista->id; ?>">
                                                                <?php echo $lista->descripcion; ?> </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_origen_fondo_descrip"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Origen de Fondos:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" required class="form-control"
                                                            id="txt_origen_fondo_descrip"
                                                            name="txt_origen_fondo_descrip" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_destino_fondo_descrip"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Destino de Fondos:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" required class="form-control"
                                                            id="txt_destino_fondo_descrip"
                                                            name="txt_destino_fondo_descrip" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group row">
                                                    <label for="txt_email"
                                                        class="col-md-4 col-form-label text-right">
                                                        <small><strong>Correo Electronico:</strong></small>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="email" autocomplete="email" required class="form-control"
                                                            id="txt_email"
                                                            name="txt_email" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <input type="hidden" class="form-control" readonly id="txt_titular_cuenta" name="txt_titular_cuenta" value="">

                            <div class="card-footer bg-white">
                                <div class="form-row">
                                    <div class="col text-left">
                                        <a href="index.php" class="btn btn-primary">
                                            <i class="fas fa-home"></i> Inicio
                                        </a>
                                        <button type="submit" class="btn btn-primary">
                                            Continuar <i class="fas fa-caret-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
    $(document).ready(function () {
        var idCliente = localStorage.getItem('usuario_id');
        $('#txt_titular_cuenta').val(idCliente);
        //OBTENER REGISTROS POR BANCOS
        recargarBancosPorCliente();
        recargarCuentasPropiasPorCliente();

        //OBTENER CUENTAS
        $('#txt_idbanco').change(function () {
            var x = $('#txt_idbanco').val();
            //alert(x);
            recargarCuentas();
            limpiarDatosBeneficiario();
        });

        //OBTENER DATOS DE LA CUENTA SELECCIONADA
        $('#txt_cuenta_destino').change(function () {
            var x = $('#txt_cuenta_destino').val();
            //alert(x);
            recargarDatosCuenta();
            limpiarDatosBeneficiario();
        });

    })
</script>
<!--AJAX-->
<script type="text/javascript">
    function recargarBancosPorCliente() {
        var idCliente = localStorage.getItem('usuario_id');
        $.ajax({
            type: "POST",
            url: "view/ajax/ListarBancosOtrosPorCliente.php",
            data: {
                idCliente
            },
            success: function (r) {
                console.log(r);
                $('#txt_idbanco').html(r);
            },
            error: function (h) {
                alert(h + "mal");
            }

        });
    }
    function recargarCuentasPropiasPorCliente() {
        var idCliente = localStorage.getItem('usuario_id');
        $.ajax({
            type: "POST",
            url: "view/ajax/listarCuentasPropiasPorCliente.php",
            data: {
                idCliente
            },
            success: function (r) {
                console.log(r);
                $('#txt_cuenta_origen').html(r);
            },
            error: function (h) {
                alert(h + "mal");
            }

        });
    }
    function recargarCuentas() {
        var idBanco = $('#txt_idbanco').val();
        var idCliente = $('#txt_titular_cuenta').val();
        $.ajax({
            type: "POST",
            url: "view/ajax/listarCuentasPorBancoPorCliente.php",
            data: {
                idBanco,
                idCliente
            },
            success: function (r) {
                console.log(r);
                $('#txt_cuenta_destino').html(r);
            },
            error: function (h) {
                alert(h + "mal");
            }

        });
    }
    function limpiarDatosBeneficiario()
    {
        $('#txt_sucursal').val("");
        $('#txt_moneda').val("");
        $('#txt_tipo_cuenta').val("");
        $('#txt_titular').val("");
        $('#txt_tipoidentificacion').val("");
        $('#txt_direccion').val("");
        $('#txt_numeroidentificacion').val("");
    }
    function recargarDatosCuenta() {
        var nroCuenta = $('#txt_cuenta_destino').val();
        $.ajax({
            type: "POST",
            url: "view/ajax/listarCuentasPorBancoPorClientePorNumCuenta.php",
            dataType:'json',
            data: {
                nroCuenta
            },
            success: function (r) {
                console.log(r);
                $('#txt_sucursal').val(r[0].sucursal);
                $('#txt_moneda').val(r[0].tipomoneda);
                $('#txt_tipo_cuenta').val(r[0].tipocuenta);
                $('#txt_titular').val(r[0].titular);
                $('#txt_tipoidentificacion').val(r[0].tipoidentificacion);
                $('#txt_direccion').val(r[0].direccion);
                $('#txt_numeroidentificacion').val(r[0].nro_identificacion);

                /*$('#txt_id_moneda').val(r[0].idtipomoneda);
                $('#txt_id_tipo_cuenta').val(r[0].idtipocuenta);
                $('#txt_id_tipoidentificacion').val(r[0].idtipoi);*/
            },
            error: function (h) {
                alert(h + "mal");
            }

        });
    }
</script>