<section class="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="index.php?cl=guardarCliente" method="POST">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <h6>Registro de Cliente</h6>
                            <small><span>Llenado de datos</span></small>
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group row">
                                                <label for="ci" class="col-md-4 col-form-label text-right">Carnet de Identidad:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="ci">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="txt_nombre" class="col-md-4 col-form-label text-right">
                                                    Nombre:
                                                </label>
                                                <div class="col-md-8">
                                                  <input type="text" id="nombre" name="nombre" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ap_paterno" class="col-md-4 col-form-label text-right">Apellido Paterno :</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="ap_paterno">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ap_materno" class="col-md-4 col-form-label text-right">Apellido Materno :</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="ap_materno">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ap_casado" class="col-md-4 col-form-label text-right">Apellido Casado:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="ap_casado">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="fecha_nacimiento" class="col-md-4 col-form-label text-right">Fecha de Nacimiento:</label>
                                                <div class="col-md-8">
                                                    <input type="date" class="form-control" name="fecha_nacimiento">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="direccion" class="col-md-4 col-form-label text-right">Direccion:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="direccion">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="correo" class="col-md-4 col-form-label text-right">Correo Electronico:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="correo_electronico">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="telefono" class="col-md-4 col-form-label text-right">Telefono:</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="telefono">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer bg-white">
                            <div class="form-row">
                                <div class="col text-left">
                                    <a href="index.php" class="btn btn-primary">
                                        <i class="fas fa-home"></i> Inicio
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        Continuar <i class="fas fa-caret-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
