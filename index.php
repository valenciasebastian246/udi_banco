<?php
    require_once('controller/principal_controller.php');
    require_once('controller/cuentaTerceroController.php');
    require_once('controller/cuentaPropiasController.php');
    require_once('controller/cuentaOtrosBancosController.php');
    require_once('controller/clienteController.php');

    include 'db/conexion.php';
    $controller= new principal_controller();
    $controllerCT= new cuentaTerceroController();
    $controllerCP= new cuentaPropiasController();
    $controllerCO= new cuentaOtrosBancosController();
    $controllerCL= new clienteController();

    
    if(!empty($_REQUEST['m'])){
        $metodo=$_REQUEST['m'];
        if (method_exists($controller, $metodo)) {
            $controller->$metodo();
        }else{
            $controller->index();
        }
    }else{
        if(!empty($_REQUEST['c'])){
            $metodo=$_REQUEST['c'];
            if (method_exists($controllerCT, $metodo)) {
                $controllerCT->$metodo();
            }else{
                $controllerCT->index();
            }  
        }else{
            if(!empty($_REQUEST['cp'])){
                $metodo=$_REQUEST['cp'];
                if (method_exists($controllerCP, $metodo)) {
                    $controllerCP->$metodo();
                }else{
                    $controllerCT->index();
                }  
            }else{
                if(!empty($_REQUEST['co'])){
                    $metodo=$_REQUEST['co'];
                    if (method_exists($controllerCO, $metodo)) {
                        $controllerCO->$metodo();
                    }else{
                        $controllerCO->index();
                    }  
                }else{
                    if(!empty($_REQUEST['cl'])){
                        $metodo=$_REQUEST['cl'];
                        if (method_exists($controllerCL, $metodo)) {
                            $controllerCL->$metodo();
                        }else{
                            $controllerCT->index();
                        }  
                    }else{
                        $controllerCT->index();
                    }
                }
            }
        }
       
    }

   

?>