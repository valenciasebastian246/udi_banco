-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-11-2021 a las 04:54:11
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbbanco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `idBanco` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estado` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`idBanco`, `nombre`, `estado`) VALUES
(1, 'Banco Ganadero', '1'),
(2, 'Banco Union', '1'),
(3, 'Prueba', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `ci` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `ap_paterno` varchar(50) NOT NULL,
  `ap_materno` varchar(50) DEFAULT NULL,
  `ap_casado` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `correo_electronico` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `estado` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `ci`, `nombre`, `ap_paterno`, `ap_materno`, `ap_casado`, `fecha_nacimiento`, `direccion`, `correo_electronico`, `telefono`, `estado`) VALUES
(1, '12335', 'Sebastian', 'Valencia', 'sOLIZ', NULL, '2020-09-01', 'av americana', 'dasdas', '78945612', '1'),
(2, '3288583', 'ADMINISTRADOR', 'Peres', 'NANA', NULL, '2020-09-01', 'khkb', 'khbkhb', '8954', '0'),
(3, '13049968', 'pRUEBA', 'Prueba', 'Prueba', '', '2021-11-06', 'abc', 'dani6.dg53@gmail.com', '75584536', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentadeposito`
--

CREATE TABLE `cuentadeposito` (
  `idCuentaDeposito` int(11) NOT NULL,
  `nroCuenta` bigint(20) NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_vencimiento` date DEFAULT NULL,
  `clienteId` int(11) NOT NULL,
  `tipoCuentaDepositoId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuentadeposito`
--

INSERT INTO `cuentadeposito` (`idCuentaDeposito`, `nroCuenta`, `fecha_registro`, `fecha_vencimiento`, `clienteId`, `tipoCuentaDepositoId`) VALUES
(43, 111, '0000-00-00 00:00:00', NULL, 1, 3),
(44, 984, '0000-00-00 00:00:00', NULL, 1, 3),
(45, 2626, '0000-00-00 00:00:00', NULL, 1, 3),
(46, 984, '0000-00-00 00:00:00', NULL, 1, 3),
(47, 756, '2020-09-20 15:56:26', NULL, 2, 1),
(48, 888, '0000-00-00 00:00:00', NULL, 1, 3),
(49, 99999, '0000-00-00 00:00:00', NULL, 1, 3),
(50, 321, '0000-00-00 00:00:00', NULL, 1, 3),
(51, 951, '0000-00-00 00:00:00', NULL, 1, 3),
(52, 983, '0000-00-00 00:00:00', NULL, 1, 3),
(53, 9999, '0000-00-00 00:00:00', NULL, 1, 3),
(54, 99, '0000-00-00 00:00:00', NULL, 1, 3),
(55, 934, '0000-00-00 00:00:00', NULL, 1, 3),
(56, 95137, '0000-00-00 00:00:00', NULL, 1, 3),
(57, 5552, '0000-00-00 00:00:00', NULL, 1, 3),
(58, 101010, '0000-00-00 00:00:00', NULL, 1, 3),
(59, 95413, '0000-00-00 00:00:00', NULL, 1, 3),
(60, 99999, '0000-00-00 00:00:00', NULL, 1, 3),
(61, 99999, '0000-00-00 00:00:00', NULL, 1, 3),
(62, 3591, '0000-00-00 00:00:00', NULL, 1, 3),
(63, 957, '0000-00-00 00:00:00', NULL, 1, 3),
(64, 95381, '0000-00-00 00:00:00', NULL, 1, 3),
(65, 95381, '0000-00-00 00:00:00', NULL, 1, 3),
(66, 9873, '0000-00-00 00:00:00', NULL, 1, 3),
(71, 754, '2020-09-29 00:00:00', '2020-10-29', 2, 2),
(72, 756, '2020-09-29 00:00:00', '2020-10-30', 2, 2),
(73, 753, '2020-10-03 00:00:00', '2020-10-03', 1, 2),
(74, 757, '2020-10-05 18:29:12', NULL, 1, 1),
(75, 1234, '0000-00-00 00:00:00', NULL, 1, 3),
(76, 111222333, '0000-00-00 00:00:00', NULL, 1, 3),
(77, 758, '2021-11-06 16:41:51', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentaotros`
--

CREATE TABLE `cuentaotros` (
  `cuentaDepositoId` int(11) NOT NULL,
  `nombre_titular` varchar(255) NOT NULL,
  `nro_identificacion` bigint(20) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `correo_electronico` varchar(100) NOT NULL,
  `bancoId` int(11) NOT NULL,
  `sucursalId` int(11) NOT NULL,
  `tipoIdentificacionId` int(11) NOT NULL,
  `tipoCuentaId` int(11) NOT NULL,
  `tipoMonedaId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuentaotros`
--

INSERT INTO `cuentaotros` (`cuentaDepositoId`, `nombre_titular`, `nro_identificacion`, `direccion`, `correo_electronico`, `bancoId`, `sucursalId`, `tipoIdentificacionId`, `tipoCuentaId`, `tipoMonedaId`) VALUES
(49, 'aas', 0, 'asd', 'gokugmail', 1, 1, 1, 1, 1),
(50, 'frezer', 21, 'Vegita', 'lol.com', 1, 1, 1, 2, 1),
(52, 'gohan', 784, 'nameku', 'dsa', 1, 1, 1, 2, 1),
(53, 'jhon', 33333, 'warnes', 'gokugmail', 1, 1, 1, 2, 2),
(55, 'klj', 1304, 'asd', 'sad', 1, 1, 1, 2, 1),
(56, 'pepepepep', 77777, 'nameku', 'dsa', 1, 1, 1, 2, 2),
(60, 'aas', 33333, 'asd', 'dsa', 1, 1, 1, 1, 1),
(61, 'pelezx', 1304, 'nameku', 'sad', 1, 1, 1, 1, 2),
(62, 'jhon depp', 9537, 'lala', 'lol.com', 1, 1, 1, 1, 1),
(65, 'pinos', 0, 'Montaña Pao', 'gokugmail', 1, 1, 1, 1, 1),
(66, 'Majin bu', 321, 'warnes', 'dsa', 1, 1, 1, 1, 1),
(75, 'pepito peres', 123456, 'sad', 'asddsa', 2, 2, 1, 1, 1),
(76, 'juan perez', 123456789, 'sad', 'sad', 2, 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentapersonal`
--

CREATE TABLE `cuentapersonal` (
  `nroCuenta` bigint(20) NOT NULL,
  `tasaInteresId` decimal(5,2) NOT NULL,
  `saldo` decimal(20,2) NOT NULL,
  `fecha_apertura` datetime NOT NULL DEFAULT current_timestamp(),
  `estado` char(1) NOT NULL DEFAULT '0',
  `clienteId` int(11) NOT NULL,
  `tipoCuentaId` int(11) NOT NULL,
  `tipoMonedaId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuentapersonal`
--

INSERT INTO `cuentapersonal` (`nroCuenta`, `tasaInteresId`, `saldo`, `fecha_apertura`, `estado`, `clienteId`, `tipoCuentaId`, `tipoMonedaId`) VALUES
(10, '2.00', '11.33', '2020-09-14 20:12:30', '1', 1, 1, 1),
(666, '2.00', '1.00', '2020-09-14 20:14:43', '0', 2, 1, 2),
(753, '2.00', '524.07', '2020-09-14 20:32:53', '0', 2, 2, 2),
(754, '10.00', '15.00', '2020-09-19 00:00:00', '0', 1, 1, 1),
(755, '2.00', '32.60', '2020-09-18 20:13:41', '0', 1, 1, 1),
(756, '2.00', '700.00', '2020-09-20 15:56:26', '1', 2, 1, 1),
(757, '2.00', '151.67', '2020-10-05 18:29:12', '0', 1, 1, 1),
(758, '1.00', '10.00', '2021-11-06 00:00:00', '0', 1, 1, 1);

--
-- Disparadores `cuentapersonal`
--
DELIMITER $$
CREATE TRIGGER `insertCuentaDeposito` AFTER INSERT ON `cuentapersonal` FOR EACH ROW BEGIN
    INSERT Into cuentadeposito (nroCuenta,clienteId,tipoCuentaDepositoId) VALUES (new.nroCuenta,new.clienteId,1);
   
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operacion_tipo_cambio`
--

CREATE TABLE `operacion_tipo_cambio` (
  `id` int(11) NOT NULL,
  `descripcion` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `operacion_tipo_cambio`
--

INSERT INTO `operacion_tipo_cambio` (`id`, `descripcion`) VALUES
(1, 'COMPRA'),
(2, 'VENTA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `idSucursal` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `idBanco` int(11) DEFAULT NULL,
  `estado` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`idSucursal`, `nombre`, `idBanco`, `estado`) VALUES
(1, '3er Anillo', 1, '1'),
(2, 'Santa Cruz bolivia', 2, '0'),
(3, 'Santa Cruz bolivia', 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocuenta`
--

CREATE TABLE `tipocuenta` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipocuenta`
--

INSERT INTO `tipocuenta` (`id`, `descripcion`) VALUES
(1, 'CAJA DE AHORRO'),
(2, 'CUENTA CORRIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocuentadeposito`
--

CREATE TABLE `tipocuentadeposito` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipocuentadeposito`
--

INSERT INTO `tipocuentadeposito` (`id`, `descripcion`) VALUES
(3, 'CUENTA A OTROS BANCOS'),
(2, 'CUENTA TERCEROS'),
(1, 'PROPIAS CUENTAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoidentificacion`
--

CREATE TABLE `tipoidentificacion` (
  `idTipoI` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estado` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipoidentificacion`
--

INSERT INTO `tipoidentificacion` (`idTipoI`, `nombre`, `estado`) VALUES
(1, 'Cedula de Identidad', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomoneda`
--

CREATE TABLE `tipomoneda` (
  `idTipoMoneda` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `abreviacion` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipomoneda`
--

INSERT INTO `tipomoneda` (`idTipoMoneda`, `descripcion`, `abreviacion`) VALUES
(1, 'BOLIVIANOS', 'N'),
(2, 'DOLARES', 'E'),
(3, 'Euro', 'e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cambio`
--

CREATE TABLE `tipo_cambio` (
  `id` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `monto_tc` decimal(15,2) NOT NULL,
  `id_operacionTC` int(11) NOT NULL,
  `id_destino_tipomoneda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_cambio`
--

INSERT INTO `tipo_cambio` (`id`, `fecha_inicio`, `fecha_fin`, `monto_tc`, `id_operacionTC`, `id_destino_tipomoneda`) VALUES
(1, '2020-10-05', NULL, '6.85', 1, 2),
(2, '2020-10-05', NULL, '6.97', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `idTransaccion` int(11) NOT NULL,
  `monto_deposito` decimal(20,2) NOT NULL,
  `glosa` varchar(50) DEFAULT NULL,
  `origenFondo` varchar(255) NOT NULL,
  `destinoFondo` varchar(255) NOT NULL,
  `fecha` datetime DEFAULT current_timestamp(),
  `estado` char(1) DEFAULT '0',
  `ClienteId` int(11) NOT NULL,
  `cuentaDebitoId` bigint(20) NOT NULL,
  `cuentaDepositoId` int(11) NOT NULL,
  `tipoMonedaId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`idTransaccion`, `monto_deposito`, `glosa`, `origenFondo`, `destinoFondo`, `fecha`, `estado`, `ClienteId`, `cuentaDebitoId`, `cuentaDepositoId`, `tipoMonedaId`) VALUES
(3, '100.00', '0', 'rr', 'rr', '2020-09-20 15:58:05', '0', 1, 756, 47, 1),
(6, '50.00', 'ff', 'nada', 'des', '2020-09-27 00:00:00', '0', 1, 10, 45, 1),
(7, '3.00', '', 'mm', 'aa', '2020-09-27 00:00:00', '0', 1, 754, 52, 1),
(8, '3.00', '', 'mm', 'aa', '2020-09-27 00:00:00', '0', 1, 754, 52, 1),
(9, '3.00', '', 'mm', 'aa', '2020-09-27 00:00:00', '0', 1, 754, 52, 1),
(10, '10.00', '', 'mm', 'aa', '2020-09-27 00:00:00', '0', 1, 10, 52, 1),
(11, '20.00', 'llll', 'mm', 'aa', '2020-09-27 00:00:00', '0', 1, 10, 50, 1),
(12, '10.00', 'aaa', 'mm', 'aa', '2020-09-27 00:00:00', '0', 1, 10, 50, 1),
(13, '1.00', 'sad', 'da', 'asd', '2020-09-27 00:00:00', '0', 1, 10, 50, 1),
(14, '1.00', 'sad', 'da', 'asd', '2020-09-27 00:00:00', '0', 1, 10, 50, 1),
(15, '1.00', 'sad', 'da', 'asd', '2020-09-27 00:00:00', '0', 1, 10, 50, 1),
(16, '100.00', 'asd', 'dsa', 'dasd', '2020-09-29 00:00:00', '0', 1, 10, 72, 1),
(17, '100.00', 'jj', 'jj', 'jjj', '2020-09-29 00:00:00', '0', 1, 10, 72, 1),
(18, '99.00', 'ss', 'sdsd', 'sdsd', '2020-09-29 00:00:00', '0', 2, 666, 71, 1),
(19, '200.00', '', 'mm', 'sss', '2020-09-29 00:00:00', '0', 1, 754, 50, 1),
(20, '10.00', 'aaa', 'mm', 'aa', '2020-09-29 00:00:00', '0', 1, 754, 50, 1),
(21, '10.00', 'kjhg', 'fd', 'ds', '2020-09-29 00:00:00', '0', 1, 754, 50, 1),
(22, '100.00', 'perjj', 'cxx', 'cxzc', '2020-10-03 00:00:00', '0', 1, 10, 73, 1),
(23, '7.00', 'khhj', 'sad', 'sda', '2020-10-03 00:00:00', '0', 1, 10, 73, 1),
(24, '10.00', '', 'df', 'ds', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(25, '10.00', 'qweq', 'sda', 'das', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(26, '10.00', 'kjhgf', 'hgfd', 'kjh', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(27, '1.00', 'ff', 'nada', 'des', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(28, '10.00', 'wadfs', 'asd', 'ads', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(29, '10.00', '', 'sfd', 'dsf', '2020-10-06 00:00:00', '0', 1, 754, 52, 1),
(30, '10.00', 'sadf', 'sdf', 'dfs', '2020-10-06 00:00:00', '0', 1, 755, 52, 1),
(31, '10.00', 'e', 'pp', 'aa', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(32, '1.00', 'sdf', 'df', 'df', '2020-10-06 00:00:00', '0', 1, 754, 52, 1),
(33, '1.00', 'kjhg', 'mm', 'aa', '2020-10-06 00:00:00', '0', 1, 757, 52, 1),
(34, '1.00', 'kjhg', 'mm', 'aa', '2020-10-06 00:00:00', '0', 1, 757, 52, 1),
(35, '1.00', 'aaa', 'fr', 're', '2020-10-06 00:00:00', '0', 1, 754, 52, 1),
(36, '1.00', 'fthy', 'mm', 'aa', '2020-10-06 00:00:00', '0', 1, 757, 50, 1),
(37, '1.00', 'fthy', 'mm', 'aa', '2020-10-06 00:00:00', '0', 1, 757, 50, 1),
(38, '1.00', 'fthy', 'mm', 'aa', '2020-10-06 00:00:00', '0', 1, 757, 50, 1),
(39, '1.00', 'fthy', 'mm', 'aa', '2020-10-06 00:00:00', '0', 1, 757, 50, 1),
(40, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 52, 1),
(41, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(42, '11.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 755, 50, 1),
(43, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(44, '10.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 755, 50, 1),
(45, '10.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 755, 50, 1),
(46, '10.00', 't', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(47, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(48, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(49, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(50, '2.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(51, '1.00', '', 'wqe', 'qwe', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(52, '2.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(53, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(54, '11.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(55, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 53, 1),
(56, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 757, 52, 1),
(57, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 757, 52, 1),
(58, '1.00', '', 'ds', 'sad', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(59, '10.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 52, 1),
(60, '1.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(61, '2.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 52, 1),
(62, '10.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 754, 50, 1),
(63, '10.00', '', '', '', '2020-10-06 00:00:00', '0', 1, 10, 52, 1),
(64, '10.00', 'ss', 'ss', 'ss', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(65, '10.00', 'dad', 'ssda', 'dsa', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(66, '10.00', 'xzc', 'cxz', 'zxc', '2020-10-06 00:00:00', '0', 1, 755, 73, 1),
(67, '1.00', 'sa', 'das', 'dsa', '2020-10-06 00:00:00', '0', 1, 754, 73, 1),
(68, '1.00', 'fad', 'fds', 'dfs', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(69, '10.00', 'dsa', 'asd', 'das', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(70, '10.00', 'asd', 'asd', 'ads', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(71, '1.00', 'df', 'df', 'fds', '2020-10-06 00:00:00', '0', 1, 754, 73, 1),
(72, '10.00', 'ds', 'das', 'asd', '2020-10-06 00:00:00', '0', 1, 10, 73, 1),
(73, '1.00', 'ads', 'dsa', 'asd', '2020-10-06 00:00:00', '0', 1, 755, 73, 1),
(74, '10.00', 'ds', 'dsa', 'ads', '2020-10-06 00:00:00', '0', 1, 755, 73, 1),
(75, '10.00', 'sa', 'asd', 'ds', '2020-10-06 00:00:00', '0', 1, 755, 74, 1),
(76, '10.00', 'sa', 'DAS', 'ADS', '2020-10-06 00:00:00', '0', 1, 755, 74, 1),
(77, '1.00', 'AS', 'SA', 'S', '2020-10-06 00:00:00', '0', 1, 754, 73, 1),
(78, '1.00', '', 'sad', 'dsa', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(79, '1.00', '', 'sad', 'dsa', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(80, '1.00', 'aaa', 'xZ', 'zx', '2020-10-06 00:00:00', '0', 1, 10, 50, 1),
(81, '5.00', 'xsz', 'xz', 'xZ', '2020-10-06 00:00:00', '0', 1, 757, 73, 1),
(82, '10.00', 'a', 'as', 'AS', '2020-10-06 00:00:00', '0', 2, 10, 73, 1),
(83, '10.00', 'xz', 'zX', 'xZ', '2020-10-06 00:00:00', '0', 1, 757, 73, 2),
(84, '15.00', '', 'mm', 'aa', '2020-10-07 00:00:00', '0', 1, 755, 50, 1),
(85, '10.00', 'fff', 'ff', 'fff', '2020-10-07 00:00:00', '0', 1, 757, 73, 1),
(86, '1.00', 'ffd', 'fdf', 'fdf', '2020-10-07 00:00:00', '0', 1, 755, 73, 1),
(87, '12.00', '', 'asd', 'das', '2020-10-07 00:00:00', '0', 1, 757, 50, 1),
(88, '1.00', 'prueba', 'prueba origen', 'prueba destino', '2020-10-10 00:00:00', '0', 1, 10, 74, 1),
(89, '12.00', '', 'mm', 'aa', '2020-10-10 00:00:00', '0', 1, 10, 76, 1),
(90, '10.00', 'ff', 'nada', 'des', '2021-11-06 00:00:00', '0', 1, 10, 74, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`idBanco`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`),
  ADD UNIQUE KEY `ci` (`ci`);

--
-- Indices de la tabla `cuentadeposito`
--
ALTER TABLE `cuentadeposito`
  ADD PRIMARY KEY (`idCuentaDeposito`),
  ADD UNIQUE KEY `clienteId` (`clienteId`,`fecha_registro`,`idCuentaDeposito`),
  ADD KEY `tipoCuentaDepositoId` (`tipoCuentaDepositoId`);

--
-- Indices de la tabla `cuentaotros`
--
ALTER TABLE `cuentaotros`
  ADD PRIMARY KEY (`cuentaDepositoId`),
  ADD KEY `bancoId` (`bancoId`),
  ADD KEY `sucursalId` (`sucursalId`),
  ADD KEY `tipoIdentificacionId` (`tipoIdentificacionId`),
  ADD KEY `tipoCuentaId` (`tipoCuentaId`),
  ADD KEY `tipoMonedaId` (`tipoMonedaId`);

--
-- Indices de la tabla `cuentapersonal`
--
ALTER TABLE `cuentapersonal`
  ADD PRIMARY KEY (`nroCuenta`),
  ADD KEY `clienteId` (`clienteId`),
  ADD KEY `tipoCuentaId` (`tipoCuentaId`),
  ADD KEY `tipoMonedaId` (`tipoMonedaId`);

--
-- Indices de la tabla `operacion_tipo_cambio`
--
ALTER TABLE `operacion_tipo_cambio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`idSucursal`),
  ADD KEY `idBanco` (`idBanco`);

--
-- Indices de la tabla `tipocuenta`
--
ALTER TABLE `tipocuenta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipocuentadeposito`
--
ALTER TABLE `tipocuentadeposito`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  ADD PRIMARY KEY (`idTipoI`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tipomoneda`
--
ALTER TABLE `tipomoneda`
  ADD PRIMARY KEY (`idTipoMoneda`);

--
-- Indices de la tabla `tipo_cambio`
--
ALTER TABLE `tipo_cambio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_destino_tipomoneda` (`id_destino_tipomoneda`,`id_operacionTC`,`fecha_inicio`),
  ADD KEY `id_operacionTC` (`id_operacionTC`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`idTransaccion`),
  ADD KEY `ClienteId` (`ClienteId`),
  ADD KEY `cuentaDebitoId` (`cuentaDebitoId`),
  ADD KEY `cuentaDepositoId` (`cuentaDepositoId`),
  ADD KEY `tipoMonedaId` (`tipoMonedaId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banco`
--
ALTER TABLE `banco`
  MODIFY `idBanco` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cuentadeposito`
--
ALTER TABLE `cuentadeposito`
  MODIFY `idCuentaDeposito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de la tabla `cuentapersonal`
--
ALTER TABLE `cuentapersonal`
  MODIFY `nroCuenta` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=759;

--
-- AUTO_INCREMENT de la tabla `operacion_tipo_cambio`
--
ALTER TABLE `operacion_tipo_cambio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `idSucursal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipocuenta`
--
ALTER TABLE `tipocuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipocuentadeposito`
--
ALTER TABLE `tipocuentadeposito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipoidentificacion`
--
ALTER TABLE `tipoidentificacion`
  MODIFY `idTipoI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tipomoneda`
--
ALTER TABLE `tipomoneda`
  MODIFY `idTipoMoneda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_cambio`
--
ALTER TABLE `tipo_cambio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `idTransaccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuentadeposito`
--
ALTER TABLE `cuentadeposito`
  ADD CONSTRAINT `cuentadeposito_ibfk_1` FOREIGN KEY (`clienteId`) REFERENCES `cliente` (`idCliente`),
  ADD CONSTRAINT `cuentadeposito_ibfk_2` FOREIGN KEY (`tipoCuentaDepositoId`) REFERENCES `tipocuentadeposito` (`id`);

--
-- Filtros para la tabla `cuentaotros`
--
ALTER TABLE `cuentaotros`
  ADD CONSTRAINT `cuentaotros_ibfk_1` FOREIGN KEY (`cuentaDepositoId`) REFERENCES `cuentadeposito` (`idCuentaDeposito`),
  ADD CONSTRAINT `cuentaotros_ibfk_2` FOREIGN KEY (`bancoId`) REFERENCES `banco` (`idBanco`),
  ADD CONSTRAINT `cuentaotros_ibfk_3` FOREIGN KEY (`sucursalId`) REFERENCES `sucursal` (`idSucursal`),
  ADD CONSTRAINT `cuentaotros_ibfk_4` FOREIGN KEY (`tipoIdentificacionId`) REFERENCES `tipoidentificacion` (`idTipoI`),
  ADD CONSTRAINT `cuentaotros_ibfk_5` FOREIGN KEY (`tipoCuentaId`) REFERENCES `tipocuenta` (`id`),
  ADD CONSTRAINT `cuentaotros_ibfk_6` FOREIGN KEY (`tipoMonedaId`) REFERENCES `tipomoneda` (`idTipoMoneda`);

--
-- Filtros para la tabla `cuentapersonal`
--
ALTER TABLE `cuentapersonal`
  ADD CONSTRAINT `cuentapersonal_ibfk_1` FOREIGN KEY (`clienteId`) REFERENCES `cliente` (`idCliente`),
  ADD CONSTRAINT `cuentapersonal_ibfk_2` FOREIGN KEY (`tipoCuentaId`) REFERENCES `tipocuenta` (`id`),
  ADD CONSTRAINT `cuentapersonal_ibfk_3` FOREIGN KEY (`tipoMonedaId`) REFERENCES `tipomoneda` (`idTipoMoneda`);

--
-- Filtros para la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD CONSTRAINT `sucursal_ibfk_1` FOREIGN KEY (`idBanco`) REFERENCES `banco` (`idBanco`);

--
-- Filtros para la tabla `tipo_cambio`
--
ALTER TABLE `tipo_cambio`
  ADD CONSTRAINT `tipo_cambio_ibfk_1` FOREIGN KEY (`id_operacionTC`) REFERENCES `operacion_tipo_cambio` (`id`),
  ADD CONSTRAINT `tipo_cambio_ibfk_2` FOREIGN KEY (`id_destino_tipomoneda`) REFERENCES `tipomoneda` (`idTipoMoneda`);

--
-- Filtros para la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `transaccion_ibfk_1` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`idCliente`),
  ADD CONSTRAINT `transaccion_ibfk_2` FOREIGN KEY (`cuentaDebitoId`) REFERENCES `cuentapersonal` (`nroCuenta`),
  ADD CONSTRAINT `transaccion_ibfk_3` FOREIGN KEY (`cuentaDepositoId`) REFERENCES `cuentadeposito` (`idCuentaDeposito`),
  ADD CONSTRAINT `transaccion_ibfk_4` FOREIGN KEY (`tipoMonedaId`) REFERENCES `tipomoneda` (`idTipoMoneda`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
