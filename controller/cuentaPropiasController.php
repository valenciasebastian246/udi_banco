<?php
include_once 'modelos/cuentaPropiasModel.php';
class cuentaPropiasController
{

    public $model;
    public $modeltransacciones;
    public function __construct()
    {
        $this->model = new cuentaPropiasModel();
        $this->modeltransacciones = new transaccionesModel();
    }

    function index()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/index.php');
        include_once('view/layout/footer.php');
    }
    function registrarCuentaPersonal()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/registro_apertura.php');
        include_once('view/layout/footer.php');
    }
    function transferencia_cuenta_propia()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/transferencia_cuenta_propia.php');
        include_once('view/layout/footer.php');
    }
    function guardarCuentaPropia()
    {

        $dato = new cuentaPropiasModel();
        $dato->nroCuenta = $_POST['cuenta'];
        $dato->tasaInteresId = $_POST['interes'];
        $dato->saldo = '0';
        $dato->fecha_apertura = $_POST['fecha'];
        $dato->estado = '0';
        $dato->clienteId = $_POST['titular'];
        $dato->tipoCuentaId = '1';
        $dato->tipoMonedaId = '1';

        $valor = $this->model->registrarCuentaPropias($dato);
        switch ($valor) {
            case '0':
                include_once('view/layout/header.php');
                include_once('view/pages/registro_apertura.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Exito!','Registro de cuenta Propia realizado con exito!','success')</script>";
                break;
            case '1':
                include_once('view/layout/header.php');
                include_once('view/pages/registro_apertura.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                break;
            case '2':
                include_once('view/layout/header.php');
                include_once('view/pages/registro_apertura.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','El nro de cuenta ya ha sido registrado!','error')</script>";
                break;
            case '3':
                include_once('view/layout/header.php');
                include_once('view/pages/registro_apertura.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','La fecha de apertura es incorrecta','error')</script>";
                break;
            default:
                include_once('view/layout/header.php');
                include_once('view/pages/registro_apertura.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','Intente nuevamente!','error')</script>";
                break;
        }
    }

    function guardarTransaccionCuentaPropias()
    {
        $cuentaDebitoId = $_POST['txt_cuenta_origen'];
        $cuentaDepositoId = $_POST['txt_cuenta_destino'];
        $MontoDeposito = $_POST['txt_monto_transferir'];
        $TipoMonedaDeposito = $_POST['txt_tipo_moneda'];
        $consulta = $this->modeltransacciones->VerificarSaldoCuenta($cuentaDebitoId, $MontoDeposito, $TipoMonedaDeposito);
        //var_dump($consulta);
        if ($consulta) {
            //var_dump($cuentaDebitoId);
            $consultaNroCuenta = $this->modeltransacciones->obtenerNroCuentaFromIdCuentaDeposito($cuentaDepositoId);
            $nroCuentaDestino = $consultaNroCuenta[0]['nroCuenta'];            
            //var_dump($nroCuentaDestino);
            $dato = new transaccionesModel();
            $dato->monto_deposito = $_POST['txt_monto_transferir'];
            $dato->glosa = $_POST['txt_glosa'];
            $dato->origenFondo = $_POST['txt_origen_fondo_descrip'];
            $dato->destinoFondo = $_POST['txt_destino_fondo_descrip'];
            $dato->ClienteId = $_POST['txt_titular_cuenta'];
            $dato->cuentaDebitoId = $_POST['txt_cuenta_origen'];
            $dato->cuentaDepositoId = $_POST['txt_cuenta_destino'];
            $dato->tipoMonedaId = $_POST['txt_tipo_moneda'];
            $tipoTransaccion = 1; //transaccion tipo CuentaPropia
            //var_dump($dato);
            $valor =  $this->modeltransacciones->registrarTransaccion($dato, $tipoTransaccion);


            if ($valor == null) {
                include_once('view/layout/header.php');
                include_once('view/pages/transferencia_cuenta_propia.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','no se pudo registrar, intente nuevamente!','error')</script>";
            } else {
                if ($valor == false) {
                    include_once('view/layout/header.php');
                    include_once('view/pages/transferencia_cuenta_propia.php');
                    include_once('view/layout/footer.php');
                    //echo '<center class="text-danger">campos vacios</center>';
                    echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                } else {
                    $monto =  $_POST['txt_monto_transferir'];
                    $idCuentaP = $_POST['txt_cuenta_origen'];
                    $idCuentaD = $nroCuentaDestino;
                    $this->modeltransacciones->ActualizacionDeSaldos($idCuentaP, $idCuentaD, $monto, $tipoTransaccion, $dato->tipoMonedaId);

                    include_once('view/layout/header.php');
                    include_once('view/pages/transferencia_cuenta_propia.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Exito!','Transaccion realizada con exito!','success')</script>";
                }
            }
        } else {
            include_once('view/layout/header.php');
            include_once('view/pages/transferencia_cuenta_propia.php');
            include_once('view/layout/footer.php');
            echo "<script type='text/javascript'>Swal.fire('Error!','Saldo insuficiente!','error')</script>";
        }
    }
}
