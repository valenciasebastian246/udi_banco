<?php
include_once 'modelos/cuenta_otros_bancos.php';
include_once 'modelos/cuenta_deposito.php';
include_once 'modelos/modelo_transacciones.php';
error_reporting(0);
class cuentaOtrosBancosController
{
    public $model;
    public $modelcd;
    public $modeltransacciones;
    public function __construct()
    {
        $this->model = new cuentaOtrosBancosModel();
        $this->modelcd = new  cuenta_deposito();
        $this->modeltransacciones = new transaccionesModel();
    }
    function index()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/index.php');
        include_once('view/layout/footer.php');
    }
    function registrarCuentaOtrosBancos()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/registro_cuentaotrosbancos.php');
        include_once('view/layout/footer.php');
    }
    function transferencia_otros()
    {
        //$idCliente = 1;
        //$listaBancosOB = $this->model->ListarBancosOtrosPorCliente($idCliente);
        //$listaCuentaPropias = $this->model->ListarCuentasPropias($idCliente);
        //var_dump($listaBancosOB);
        include_once('view/layout/header.php');
        include_once('view/pages/transferencia_otros.php');
        include_once('view/layout/footer.php');
    }
    function guardarCuentaOtrosBancos()
    {
        $dato_cd = new cuenta_deposito();
        $dato_cd->nroCuenta = $_POST['txt_num_cuenta'];
        $dato_cd->fecha_vencimiento = $_POST['txt_fecha_vencimiento'];
        $dato_cd->clienteId = $_POST['txt_titular_cuenta'];
        $dato_cd->tipoCuentaDepositoId = '3'; //otros bancos

        $valor_cd = $this->modelcd->registrarTipoCuentaOtrosBancos($dato_cd);

        if (($valor_cd != 1) && ($valor_cd != 0)) {
            $dato = new cuentaOtrosBancosModel();
            $dato->cuentaDepositoId = $valor_cd;
            $dato->nombre_titular = $_POST['txt_titular'];
            $dato->nro_identificacion = $_POST['txt_numero_identificacion'];
            $dato->direccion = $_POST['txt_direccion'];
            $dato->correo_electronico = $_POST['txt_correo'];
            $dato->bancoId = $_POST['txt_idbanco'];
            $dato->sucursalId = $_POST['txt_idsucursal'];
            $dato->tipoIdentificacionId = $_POST['txt_tipo_identificacion'];
            $dato->tipoCuentaId = $_POST['txt_tipo_cuenta'];
            $dato->tipoMonedaId = $_POST['txt_moneda'];

            $valor = $this->model->registrarCuentaOtrosBancos($dato);
            switch ($valor) {
                case '0':
                    include_once('view/layout/header.php');
                    include_once('view/pages/registro_cuentaotrosbancos.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Exito!','Registro de cuenta de Otro Banco realizado con exito!','success')</script>";
                    break;
                case '1':
                    include_once('view/layout/header.php');
                    include_once('view/pages/registro_cuentaotrosbancos.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                    break;
                case '2':
                    include_once('view/layout/header.php');
                    include_once('view/pages/registro_cuentaotrosbancos.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Error!','El nro de cuenta no existe en base de datos, intente nuevamente!','error')</script>";
                    break;
                default:
                    include_once('view/layout/header.php');
                    include_once('view/pages/registro_cuentaotrosbancos.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Error!','Intente nuevamente!','error')</script>";
                    break;
            }
        } else {
            include_once('view/layout/header.php');
            include_once('view/pages/registro_cuentaotrosbancos.php');
            include_once('view/layout/footer.php');
            echo "<script type='text/javascript'>Swal.fire('Error!','Intente nuevamente!','error')</script>";
          
        }
    }
    function guardarTransaccionCuentaOtrosBancos()
    {
        $cuentaDebitoId = $_POST['txt_cuenta_origen'];
        $cuentaDepositoId = $_POST['txt_cuenta_destino'];
        $MontoDeposito = $_POST['txt_monto_transferir'];
        $TipoMonedaDeposito = $_POST['txt_tipo_moneda'];
        $consulta = $this->modeltransacciones->VerificarSaldoCuenta($cuentaDebitoId, $MontoDeposito, $TipoMonedaDeposito);
        if ($consulta) {

            $dato = new transaccionesModel();
            $dato->monto_deposito = $_POST['txt_monto_transferir'];
            $dato->glosa = $_POST['txt_glosa'];
            $dato->origenFondo = $_POST['txt_origen_fondo_descrip'];
            $dato->destinoFondo = $_POST['txt_destino_fondo_descrip'];
            $dato->ClienteId = $_POST['txt_titular_cuenta'];
            $dato->cuentaDebitoId = $_POST['txt_cuenta_origen'];
            $dato->cuentaDepositoId = $_POST['txt_cuenta_destino'];
            $dato->tipoMonedaId = $_POST['txt_tipo_moneda'];
            $tipoTransaccion = 3; //transaccion tipo Otros Bancos
            /*var_dump($dato);
            $valor = null;*/
            $valor =  $this->modeltransacciones->registrarTransaccion($dato, $tipoTransaccion);


            if ($valor == null) {
                include_once('view/layout/header.php');
                include_once('view/pages/transferencia_otros.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','no se pudo registrar, intente nuevamente!','error')</script>";
            } else {
                if ($valor == false) {
                    include_once('view/layout/header.php');
                    include_once('view/pages/transferencia_otros.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                } else {
                    $monto =  $_POST['txt_monto_transferir'];
                    $idCuentaP = $_POST['txt_cuenta_origen'];
                    $idCuentaD = $_POST['txt_cuenta_destino'];
                    $this->modeltransacciones->ActualizacionDeSaldos($idCuentaP, $idCuentaD, $monto, $tipoTransaccion, $dato->tipoMonedaId);

                    include_once('view/layout/header.php');
                    include_once('view/pages/transferencia_otros.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Exito!','Transaccion realizada con exito!','success')</script>";
                }
            }
        } else {
            include_once('view/layout/header.php');
            include_once('view/pages/transferencia_otros.php');
            include_once('view/layout/footer.php');
            echo "<script type='text/javascript'>Swal.fire('Error!','Saldo insuficiente!','error')</script>";
        }
    }
}
