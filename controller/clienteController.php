<?php 
include_once 'modelos/modelo_cliente.php';

 class clienteController{

     public $model;
     public function __construct()
        {
            $this->model = new clienteModel();
        }

    function index(){
        include_once('view/layout/header.php');
        include_once('view/pages/index.php');
        include_once('view/layout/footer.php');
    }
    function registrarCliente(){
        include_once('view/layout/header.php');
        include_once('view/pages/registro_cliente.php');
        include_once('view/layout/footer.php');
    }
    function guardarCliente(){
        
        $dato = new clienteModel();
        $dato->ci = $_POST['ci'];
        $dato->nombre = $_POST['nombre'];
        $dato->ap_paterno = $_POST['ap_paterno'];
        $dato->ap_materno = $_POST['ap_materno'];
        $dato->ap_casado = $_POST['ap_casado'];
        $dato->fecha_nacimiento = $_POST['fecha_nacimiento'];
        $dato->direccion = $_POST['direccion'];
        $dato->correo_electronico = $_POST['correo_electronico'];
        $dato->telefono = $_POST['telefono'];
        $dato->estado = '0';
        
        $valor= $this->model->registrarClientes($dato);
        switch ($valor) {
            case '0':
                include_once('view/layout/header.php');
                include_once('view/pages/index.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Exito!','Registro realizada con exito!','success')</script>";
                break;
            case 1:
                include_once('view/layout/header.php');
                include_once('view/pages/registro_cliente.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                break;
            case 2:
                include_once('view/layout/header.php');
                include_once('view/pages/registro_cliente.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','La fecha es incorrecta!','error')</script>";
                break;
            case 3:
                include_once('view/layout/header.php');
                include_once('view/pages/registro_cliente.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','El numero de carnet ya ha sido registrado!','error')</script>";
                break;
            default:
                include_once('view/layout/header.php');
                include_once('view/pages/registro_cliente.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Error!','no se pudo registrar, intente nuevamente!','error')</script>";
                break;
        }
    }
 }
?>