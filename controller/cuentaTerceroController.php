<?php
include_once 'modelos/cuenta_terceros.php';
include_once 'modelos/cuenta_deposito.php';

//error_reporting(0);
class cuentaTerceroController
{

    public $model;
    public $modelcd;
    public $model_Transacciones;
    public function __construct()
    {
        $this->model = new cuentaTercero();
        $this->modelcd = new  cuenta_deposito();
        $this->model_Transacciones = new transaccionesModel();
    }
    function index()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/index.php');
        include_once('view/layout/footer.php');
    }
    function registrarCuentaTerceros()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/registro_cuentaterceros.php');
        include_once('view/layout/footer.php');
    }
    function transferencia_terceros()
    {
        include_once('view/layout/header.php');
        include_once('view/pages/transferencia_terceros.php');
        include_once('view/layout/footer.php');
    }

    function guardarTransaccionCuentaTerceros2()
    {
        $dato = new cuentaTercero();
        $dato->monto_deposito = $_POST['txt_monto_transferir'];
        $dato->glosa = $_POST['txt_glosa'];
        $dato->origenFondo = $_POST['txt_origen_fondo_descrip'];
        $dato->destinoFondo = $_POST['txt_destino_fondo_descrip'];
        $dato->ClienteId = $_POST['txt_titular_cuenta_debito'];
        $dato->cuentaDebitoId = $_POST['txt_cuenta_origen'];
        $dato->cuentaDepositoId = $_POST['txt_cuenta_destino'];
        $dato->tipoMonedaId = $_POST['txt_tipo_moneda'];

        // var_dump($dato);
        $valor = $this->model->registrarTransaccion($dato);

        if ($valor == null) {
            include_once('view/layout/header.php');
            include_once('view/pages/transferencia_terceros.php');
            include_once('view/layout/footer.php');

            //echo '<center class="text-danger">campos vacios</center>';
            echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
        }else{
            if ($valor == false) {
                include_once('view/layout/header.php');
                include_once('view/pages/transferencia_terceros.php');
                include_once('view/layout/footer.php');
                //echo '<center class="text-danger">campos vacios</center>';
                echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
            } else {
                $monto =  $_POST['txt_monto_transferir'];
                $idCuentaP = $_POST['txt_cuenta_origen'];
                $idCuentaD = $_POST['txt_cuenta_destino'];
                $this->model_Transacciones->ActualizacionDeSaldos($idCuentaP, $idCuentaD, $monto, $tipoTransaccion, $dato->tipoMonedaId);
                include_once('view/layout/header.php');
                include_once('view/pages/index.php');
                include_once('view/layout/footer.php');
                echo "<script type='text/javascript'>Swal.fire('Exito!','Transaccion realizada con exito!','success')</script>";
            }
        } 
          

        
    }
    function guardarTransaccionCuentaTerceros()
    {
        $cuentaDebitoId = $_POST['txt_cuenta_origen'];
        $cuentaDepositoId = $_POST['txt_cuenta_destino'];
        $MontoDeposito = $_POST['txt_monto_transferir'];
        $TipoMonedaDeposito = $_POST['txt_tipo_moneda'];
        if($cuentaDebitoId!='Selecciona una cuenta' && $cuentaDepositoId!='seleccione una cuenta de destino' &&
        $cuentaDepositoId!='Sin cuentas de destino' && $TipoMonedaDeposito!='Seleccione la moneda'){

      
        $consulta = $this->model_Transacciones->VerificarSaldoCuenta($cuentaDebitoId, $MontoDeposito, $TipoMonedaDeposito);
        if ($consulta) {
            $consultaNroCuenta = $this->model_Transacciones->obtenerNroCuentaFromIdCuentaDeposito($cuentaDepositoId);
            $nroCuentaDestino = $consultaNroCuenta[0]['nroCuenta'];



            $dato = new transaccionesModel();
            $dato->monto_deposito = $_POST['txt_monto_transferir'];
            $dato->glosa = $_POST['txt_glosa'];
            $dato->origenFondo = $_POST['txt_origen_fondo_descrip'];
            $dato->destinoFondo = $_POST['txt_destino_fondo_descrip'];
            $dato->ClienteId = $_POST['txt_titular_cuenta_debito'];
            $dato->cuentaDebitoId = $cuentaDebitoId;
            $dato->cuentaDepositoId = $_POST['txt_cuenta_destino'];
            $dato->tipoMonedaId = $_POST['txt_tipo_moneda'];
            $tipoTransaccion = 2; //transaccion tipo CuentaPropia

            //var_dump($dato);
            $valor = $this->model_Transacciones->registrarTransaccion($dato, $tipoTransaccion);

            if ($valor == null) {
                include_once('view/layout/header.php');
                include_once('view/pages/transferencia_terceros.php');
                include_once('view/layout/footer.php');

                //echo '<center class="text-danger">campos vacios</center>';
                echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
            } else {
                if ($valor == false) {
                    include_once('view/layout/header.php');
                    include_once('view/pages/transferencia_terceros.php');
                    include_once('view/layout/footer.php');
                    //echo '<center class="text-danger">campos vacios</center>';
                    echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                } else {
                    $monto =  $_POST['txt_monto_transferir'];
                    $idCuentaP = $_POST['txt_cuenta_origen'];
                    $idCuentaD = $nroCuentaDestino;
                    $this->model_Transacciones->ActualizacionDeSaldos($idCuentaP, $idCuentaD, $monto, $tipoTransaccion, $dato->tipoMonedaId);
                    include_once('view/layout/header.php');
                    include_once('view/pages/index.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Exito!','Transaccion realizada con exito!','success')</script>";
                }
            }
        } else {
            include_once('view/layout/header.php');
            include_once('view/pages/transferencia_terceros.php');
            include_once('view/layout/footer.php');
            echo "<script type='text/javascript'>Swal.fire('Error!','Saldo insuficiente!','error')</script>";
        }

     }else{
        include_once('view/layout/header.php');
        include_once('view/pages/transferencia_terceros.php');
        include_once('view/layout/footer.php');
        
        //echo '<center class="text-danger">campos vacios</center>';
        echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
     }
}

    



    function guardarCuentaTerceros(){
        $dato_cuenta_deposito= new cuenta_deposito();
        $dato_cuenta_deposito->nroCuenta= $_POST['txt_num_cuenta'];
        $dato_cuenta_deposito->fecha_vencimiento= $_POST['txt_fecha_vencimiento'];
        $dato_cuenta_deposito->clienteId= $_POST['txt_titular_cuenta_debito'];
        $dato_cuenta_deposito->tipoCuentaDepositoId= '2';
        $count= $this->modelcd->verificarCuentaTerceroExistente($dato_cuenta_deposito);
            //var_dump( $count->count_nro_cuenta);
                //var_dump($count);
                if($count>=1){
                    include_once('view/layout/header.php');
                    include_once('view/pages/registro_cuentaterceros.php');
                    include_once('view/layout/footer.php');
                    echo "<script type='text/javascript'>Swal.fire('Error!','ya tienes registrada esta cuenta de terceros!','error')</script>";
                }else{
                    $valor_cd= $this->modelcd->registrarTipoCuentaTerceros($dato_cuenta_deposito);
                    if($valor_cd==1){
                     include_once('view/layout/header.php');
                     include_once('view/pages/registro_cuentaterceros.php');
                     include_once('view/layout/footer.php');
                     echo "<script type='text/javascript'>Swal.fire('Error!','campos vacios!','error')</script>";
                    }else if($valor_cd==0){
                         include_once('view/layout/header.php');
                         include_once('view/pages/index.php');
                         include_once('view/layout/footer.php');
                         echo "<script type='text/javascript'>Swal.fire('Exito!','Registro de cuenta terceros realizada con exito!','success')</script>";
                     }else{
                         if($valor_cd==2){
                             include_once('view/layout/header.php');
                             include_once('view/pages/registro_cuentaterceros.php');
                             include_once('view/layout/footer.php');
                             echo "<script type='text/javascript'>Swal.fire('Error!','El nro de cuenta no existe en base de datos, intente nuevamente!','error')</script>";  
                         
                         }
                     }
                }
      
        
    }

}