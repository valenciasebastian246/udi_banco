<?php 
class clienteModel{

    public $conexion;

    public $idCliente;
    public $ci;
    public $nombre;
    public $ap_paterno;
    public $ap_materno;
    public $ap_casado;
    public $fecha_nacimiento;
    public $direccion;
    public $correo_electronico;
    public $telefono;
    public $estado;

    public function __construct()
    {
        try {
            $this->conexion = Database::connect();
        } catch (Exception $e) {
            die($e->getMessage() + " ERRROR_construct_clienteModel");
        }
    }

    public function registrarClientes(clienteModel $data)
    {
        try {
            $fechaActual = date("Y-m-d"); 

            $query = "INSERT into cliente (ci,nombre,ap_paterno,ap_materno,ap_casado,
            fecha_nacimiento,direccion,correo_electronico,telefono,estado) values(?,?,?,?,?,?,?,?,?,?)";

            $queryc = "SELECT ci FROM cliente WHERE ci='$data->ci'";
            $str= $this->conexion->prepare($queryc);
            $str->execute();
            $respuesta= $str->fetchAll(PDO::FETCH_ASSOC);
            $count= $str->rowCount();

            if($count>0){   
                return 3; 
            }else{
                if($data->fecha_nacimiento >= $fechaActual){
                    return 2;
                }else{
                
                    if ($data->ci != null && $data->nombre != null && $data->ap_paterno != null && $data->ap_materno != null && 
                    $data->fecha_nacimiento != null && $data->direccion != null && $data->correo_electronico != null && $data->telefono != null ) {
                    
                        $resp = $this->conexion->prepare($query)->execute(
                        array(
                            $data->ci, $data->nombre, $data->ap_paterno, $data->ap_materno, $data->ap_casado,
                            $data->fecha_nacimiento, $data->direccion, $data->correo_electronico, $data->telefono, $data->estado
                        )
                    );
                    return 0;
                    } else {
                        return 1;
                    }
                }
            }
        } catch (Exception $e) {
            die($e->getMessage() + "error en registrarCliente()");
        }
    }
}
?>