<?php
class cuentaOtrosBancosModel
{
    public $cuentaDepositoId;
    public $nombre_titular;
    public $nro_identificacion;
    public $direccion;
    public $correo_electronico;
    public $bancoId;
    public $sucursalId;
    public $tipoIdentificacionId;
    public $tipoCuentaId;
    public $tipoMonedaId;

    public function __construct()
    {
        try {
            $this->conexion = Database::connect();
        } catch (Exception $e) {
            die($e->getMessage() + " ERRROR_construct_cuenta_otros_bancos");
        }
    }
    public function ListarBancosOtros()
    {
        try {

            $query = "SELECT idBanco AS id, nombre FROM banco WHERE estado = 0 AND idBanco <> 1";

            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en listarBancos()");
        }
    }
    public function ListarSucursalesXBanco($id_banco)
    {
        try {
            $query = "SELECT idSucursal AS id, nombre FROM sucursal 
         WHERE idBanco = " . $id_banco . " AND estado = 0";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en listarSucursalesXBanco()");
        }
    }
    public function ListarTipoCuentas()
    {
        try {
            $query = "SELECT id, descripcion FROM tipoCuenta";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en listarTipoCuentas()");
        }
    }
    public function ListarTipoIdentificacion()
    {
        try {
            $query = "SELECT idTipoI AS id, nombre FROM tipoidentificacion WHERE estado = 0";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en listarTipoIdentificacion()");
        }
    }
    public function ListarTipoMoneda()
    {
        try {
            $query = "SELECT idTipoMoneda AS id, descripcion, abreviacion FROM tipomoneda";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en listarTipoIdentificacion()");
        }
    }
    public function registrarCuentaOtrosBancos(cuentaOtrosBancosModel $data)
    {
        try {
            $query = "INSERT into cuentaotros (
                cuentaDepositoId,
                nombre_titular,
                nro_identificacion,
                direccion,
                correo_electronico,
                bancoId,
                sucursalId,
                tipoIdentificacionId,
                tipoCuentaId,
                tipoMonedaId)
            values(?,?,?,?,?,?,?,?,?,?)";

            if ($data->cuentaDepositoId != null 
                && $data->nombre_titular != null
                && $data->nro_identificacion != null
                && $data->direccion != null
                && $data->correo_electronico != null
                && $data->bancoId != null
                && $data->sucursalId != null
                && $data->tipoIdentificacionId != null
                && $data->tipoCuentaId != null
                && $data->tipoMonedaId != null
                ) {
                if ($data->cuentaDepositoId != null) {
                    try {
                        $resp = $this->conexion->prepare($query);
                        $resp->execute(
                            array(
                                $data->cuentaDepositoId,
                                $data->nombre_titular,
                                $data->nro_identificacion,
                                $data->direccion,
                                $data->correo_electronico,
                                $data->bancoId,
                                $data->sucursalId,
                                $data->tipoIdentificacionId,
                                $data->tipoCuentaId,
                                $data->tipoMonedaId
                            )
                        );
                    } catch (Exception $e) {
                        die($e->getMessage() + " error en registrar OtrosBancos()");
                    }
                } else {
                    return 2;
                }
                return 0;
            } else {
                return 1;
            }
        } catch (Exception $e) {
            die($e->getMessage() + "error en registrarCuentaOtrosBancos()");
        }
    }
    public function ListarBancosOtrosPorCliente($idCliente)
    {
        try {
            $query = "SELECT b.idBanco AS id, b.nombre
            FROM banco AS b, cuentaotros AS co, cuentadeposito AS cd
            WHERE co.bancoid = b.idBanco
            AND cd.idcuentadeposito = co.cuentadepositoid
            AND cd.clienteid = ".$idCliente."
            GROUP BY id";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en ListarBancosOtrosPorCliente()");
        }
    }
    public function ListarCuentasPropias($idCliente)
    {
        try {
            $query = "SELECT cp.nrocuenta  AS id, CONCAT('M/',tm.abreviacion,' ',cd.nrocuenta,' ',tc.descripcion) AS descripcion
            FROM cuentapersonal AS cp, 
            cuentadeposito AS cd,
            tipocuenta AS tc,
            tipomoneda AS tm
            WHERE cp.clienteid = ".$idCliente."
            AND cp.nrocuenta = cd.nrocuenta
            AND cd.tipocuentadepositoid = 1
            AND cp.tipocuentaid = tc.id
            AND cp.tipomonedaid = tm.idtipomoneda";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + " error en ListarCuentasPropias()");
        }
    }
}
