<?php
class transaccionesModel
{

    public $conexion;

    public $idTransaccion;
    public $monto_deposito;
    public $glosa;
    public $origenFondo;
    public $destinoFondo;
    public $fecha;
    public $estado;
    public $ClienteId;
    public $cuentaDebitoId;
    public $cuentaDepositoId;
    public $tipoMonedaId;

    public function __construct()
    {
        try {
            $this->conexion = Database::connect();
        } catch (Exception $e) {
            die($e->getMessage() + " ERRROR_construct_transaccionesModel");
        }
    }
    public function registrarTransaccion(transaccionesModel $data, $tipoTransaccion)
    {
        try {
            $query = "INSERT into transaccion (monto_deposito,glosa,origenFondo,destinoFondo,ClienteId,
            cuentaDebitoId,cuentaDepositoId,tipoMonedaId)
            values(?,?,?,?,?,?,?,?)";
            if (
                $data->monto_deposito != null && $data->glosa != null && $data->origenFondo != null && $data->destinoFondo != null &&
                $data->ClienteId != 'selecciona un titular' && $data->cuentaDebitoId != 'Selecciona una cuenta' &&
                $data->cuentaDepositoId != 'seleccione una cuenta de destino'&& $data->cuentaDepositoId != 'Sin cuentas de destino' 
                 && $data->tipoMonedaId != 'Seleccione la moneda'
            ) {
                $resp = $this->conexion->prepare($query)->execute(
                    array(
                        $data->monto_deposito, $data->glosa, $data->origenFondo, $data->destinoFondo,
                        $data->ClienteId, $data->cuentaDebitoId, $data->cuentaDepositoId, $data->tipoMonedaId
                    )
                );
              
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            die($e->getMessage() + "error en registrarTransaccion()");
        }
    }
    public function ActualizacionDeSaldos($idCuentaP, $idCuentaD, $Monto, $tipoTransaccion, $tipoMonedaId)
    {
        /*
        TIPOMONEDA          
            [1: BS,         2: $us]
        TIPOPROCESOCAMBIO   
            [1: COMPRA,     2: VENTA ]
        TIPO TRANSACCION    
            [1: PROPIAS,    2: TERCEROS, 3: OTROSBANCOS]
        */

        try {
            //DEPENDE DEL TIPO DE MONEDA ORIGEN-DESTINO DEPOSITO
            $tipoTransaccion = $tipoTransaccion;
            $TipoProcesoCambio = 2;
            $montoDescuento = 0;            
            $montoAbono = 0;
            
            $tipoCambioDatosDolar = $this->obtenerTCMoneda( 2, $TipoProcesoCambio);
            $tipoCambio = $tipoCambioDatosDolar[0]['monto_tc'];

            $tm_dato_origen = $this->obtenerTipoMonedaCuentaPersonal($idCuentaP);
            $MonedaOrigen = $tm_dato_origen[0]['tipomonedaid'];

            if ($MonedaOrigen == $tipoMonedaId ) {
                $montoDescuento = $Monto;
            } else 
            {
                $montoDescuento = $this->convertirDivisa($Monto, $MonedaOrigen, $tipoMonedaId, 2);          
            }   
            
            //RESTA EL MONTO A LA CUENTA QUE HIZO LA TRANSFERENCIA
            $query = "UPDATE cuentapersonal as cp set cp.saldo = cp.saldo-'$montoDescuento' where cp.nroCuenta='$idCuentaP' ";
            $str = $this->conexion->prepare($query);
            $str->execute();

            if ($tipoTransaccion != 3)
            {
                $tm_dato_destino = $this->obtenerTipoMonedaCuentaPersonal($idCuentaD);
                $MonedaDestino = $tm_dato_destino[0]['tipomonedaid'];
                                
                if ($MonedaDestino == $tipoMonedaId) {
                    $montoAbono = $Monto;
                } else {
                    $montoAbono = $this->convertirDivisa($Monto, $MonedaDestino, $tipoMonedaId, 1);
                }

                //SUMA EL MONTO  A LA CUENTA TRANSFERIDA
                $queryS = "UPDATE cuentapersonal as cp set cp.saldo = cp.saldo+'$montoAbono' where cp.nroCuenta='$idCuentaD'";
                $strS = $this->conexion->prepare($queryS);
                $strS->execute();
            }
            
        } catch (Exception $e) {
            die($e->getMessage() + "error en ActualizacionDeSaldos()");
        }
    }

    public function listarUltimasTransacciones()
    {
        try {
            $query = "SELECT CONCAT(cp.nroCuenta,' ',tc.descripcion,' M/',tm.abreviacion) as origenCuenta, transaccion.monto_deposito,
                    transaccion.glosa, transaccion.fecha,CONCAT(c.nombre,' ',c.ap_paterno,' ',c.ap_materno)as nombreCliente, 
                    CONCAT(cd.nroCuenta,' ',tc.descripcion,' M/',tm.abreviacion) as CuentaDeposito, 
                    tm.descripcion as tipoMoneda
                    FROM transaccion INNER JOIN cuentapersonal as cp ON
                     transaccion.cuentaDebitoId=cp.nroCuenta
                    INNER JOIN cliente as c ON transaccion.ClienteId=c.idCliente
                    INNER JOIN tipocuenta as tc ON cp.tipoCuentaId=tc.id 
                    INNER join tipomoneda as tm on transaccion.tipoMonedaId= tm.idTipoMoneda
                    INNER JOIN cuentadeposito as cd on transaccion.cuentaDepositoId=cd.idCuentaDeposito
                    where DATE_FORMAT(transaccion.fecha, '%Y-%m-%d') = CURDATE() and transaccion.estado=0";
            $str = $this->conexion->prepare($query);
            $str->execute();
            //$res=$str->fetchAll(PDO::FETCH_OBJ);
            //var_dump($res);
            return $str->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage() + "error en listarUltimasTransacciones()");
        }
    }
    public function obtenerTipoMonedaCuentaPersonal($nroCuenta)
    {
        try {
            $query = "SELECT tipomonedaid FROM cuentapersonal WHERE nrocuenta = ".$nroCuenta." ";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage() + "error en obtenerTipoMonedaCuentaPersonal()");
        }
    }
    
    public function obtenerTCMoneda($MonedaDestino, $TipoProcesoCambio)
    {
        try {
            $query = "SELECT monto_tc FROM tipo_cambio WHERE id_destino_tipomoneda = ".$MonedaDestino." AND id_operacionTC = ".$TipoProcesoCambio." ";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage() + "error en obtenerTipoMonedaCuentaDestino()");
        }
    }
    public function convertirDivisa($Monto, $MonedaOrigen, $MonedaDestino, $TipoProcesoCambio)
    {
        /*
        TIPOMONEDA 
        1 = BS
        2 = $us
        TIPOPROCESOCAMBIO
        1 = COMPRA
        2 = VENTA
         */
        $nuevoMonto = 0;
        $tipoCambioDatosDolar = $this->obtenerTCMoneda( 2, $TipoProcesoCambio);
        $tipoCambio = $tipoCambioDatosDolar[0]['monto_tc'];
        //$TipoProcesoCambio => No estamos usando esta variable

        if ($MonedaOrigen == $MonedaDestino) {
            $nuevoMonto = $Monto;
        }
        if ($MonedaOrigen == 1 && $MonedaDestino == 2) {
            $nuevoMonto = $Monto * $tipoCambio;
        }
        if ($MonedaOrigen == 2 && $MonedaDestino == 1) {
            $nuevoMonto = $Monto / $tipoCambio;
        }       
        return $nuevoMonto;
    }
    public function VerificarSaldoCuenta($nroCuenta, $MontoDeposito, $TipoMonedaDeposito)
    {
        try {
            $query = "SELECT saldo FROM cuentapersonal WHERE nroCuenta = " . $nroCuenta . " ";
            //var_dump($query);
            $str = $this->conexion->prepare($query);
            $str->execute();
            $consulta = $str->fetchAll(PDO::FETCH_ASSOC);
            $saldoCuenta = $consulta[0]['saldo'];
            if($saldoCuenta > 0)
            {
                $tm_dato = $this->obtenerTipoMonedaCuentaPersonal($nroCuenta);
                $MonedaCuenta= $tm_dato[0]['tipomonedaid'];
                if ($MonedaCuenta == $TipoMonedaDeposito) {
                    if ($MontoDeposito <= $saldoCuenta) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    $montoDescuento = $this->convertirDivisa($MontoDeposito, $MonedaCuenta, $TipoMonedaDeposito, 2);
                    if ($montoDescuento <= $saldoCuenta) {
                        return true;
                    } else {
                        return false;
                    }                    
                }                
            } else {
                return false;
            }            
        } catch (Exception $e) {
            die($e->getMessage() + "error en VerificarSaldoCuenta()");
        }
    }
    public function obtenerNroCuentaFromIdCuentaDeposito($idCuentaDeposito)
    {
        try {
            $query = "select nroCuenta from cuentadeposito WHERE idCuentaDeposito = " . $idCuentaDeposito . " ";
            $str = $this->conexion->prepare($query);
            $str->execute();
            return $str->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage() + "error en VerificarSaldoCuenta()");
        }
    }
    
}