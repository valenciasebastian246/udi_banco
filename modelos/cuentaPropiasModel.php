<?php
class cuentaPropiasModel{
    
    public $cuentaDepositoId;
    public $nombre_titular;
    public $nro_identificacion;
    public $direccion;
    public $correo_electronico;
    public $bancoId;
    public $sucursalId;
    public $tipoIdentificacionId;
    public $tipoCuentaId;
    public $tipoMonedaId;

    public function __construct()
    {
        try {
            $this->conexion = Database::connect();
        } catch (Exception $e) {
            die($e->getMessage()+" ERRROR_construct_cuenta_propas");
        }
    }
    public function registrarCuentaPropias(cuentaPropiasModel $data)
    {
        try {

            $fechaActual = date("Y-m-d"); 

            $query = "INSERT into cuentapersonal (
                nroCuenta,
                tasaInteresId,
                saldo,
                fecha_apertura,
                estado,
                clienteId,
                tipoCuentaId,
                tipoMonedaId)
            values(?,?,?,?,?,?,?,?)";

                $queryc = "SELECT nroCuenta FROM cuentapersonal WHERE nroCuenta='$data->nroCuenta'";
                $str= $this->conexion->prepare($queryc);
                $str->execute();
                $respuesta= $str->fetchAll(PDO::FETCH_ASSOC);
                $count= $str->rowCount();

                if($count>0){
                    return 2;
                }else{
                    if($data->fecha_apertura < $fechaActual){
                        return 3;
                    }else{
                        if ($data->nroCuenta != null && $data->tasaInteresId != null && $data->saldo != null
                        && $data->fecha_apertura != null && $data->estado != null && $data->clienteId != null
                        && $data->tipoCuentaId != null && $data->tipoMonedaId != null){
                        
                            $resp = $this->conexion->prepare($query);
                            $resp->execute(
                                array(
                                    $data->nroCuenta,
                                    $data->tasaInteresId,
                                    $data->saldo,
                                    $data->fecha_apertura,
                                    $data->estado,
                                    $data->clienteId,
                                    $data->tipoCuentaId,
                                    $data->tipoMonedaId
                                )
                            );
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                        
                }
                
        } catch (Exception $e) {
            die($e->getMessage() + "error en registrarCuentaPersonal()");
        }
    }

    public function registrarTransaccion(cuentaPropiasModel $data){
        try{
            $query = "INSERT into transaccion (monto_deposito,glosa,origenFondo,destinoFondo,ClienteId,
            cuentaDebitoId,cuentaDepositoId,tipoMonedaId)
            values(?,?,?,?,?,?,?,?)";
            //var_dump($data);
            if($data->monto_deposito!=null && $data->glosa!=null && $data->origenFondo !=null && $data->destinoFondo !=null && 
            $data->ClienteId !='selecciona un titular' && $data->cuentaDebitoId !='Selecciona la Cuenta de Débito' && 
            $data->cuentaDepositoId !='seleccione una cuenta de destino' && $data->tipoMonedaId!='Seleccione la moneda'){
               $resp= $this->conexion->prepare($query)->execute(
                    array($data->monto_deposito,$data->glosa,$data->origenFondo,$data->destinoFondo,
                    $data->ClienteId,$data->cuentaDebitoId,$data->cuentaDepositoId,$data->tipoMonedaId));
                
    
                return true;  
                  
            }else{
                return false;
            } 
            
           }catch(Exception $e){
               die($e->getMessage()+"error en registrarTransaccion()");
    
           }     
       }

       public function ActualizacionDeSaldos($idCuentaP, $idCuentaD, $monto){
        try{
            //RESTA EL MONTO A LA CUENTA QUE HIZO LA TRANSFERENCIA
            $query = "UPDATE cuentapersonal as cp set cp.saldo = cp.saldo-'$monto' where cp.nroCuenta='$idCuentaP' ";
            $str= $this->conexion->prepare($query);
            $str->execute();
            
            //SUMA EL MONTO  A LA CUENTA TRANSFERIDA
           $queryS = "UPDATE cuentapersonal as cp, cuentadeposito as cd set cp.saldo = cp.saldo+'$monto' where cd.idCuentaDeposito='$idCuentaD' and cp.nroCuenta= cd.nroCuenta ";
            $strS= $this->conexion->prepare($queryS);
            $strS->execute();
    
    
           }catch(Exception $e){
               die($e->getMessage()+"error en ActualizacionDeSaldos()");
    
           }  
       }

       public function listarTipoMoneda(){
        try{
            $query = "SELECT * FROM tipoMoneda";
            $str= $this->conexion->prepare($query);
            $str->execute();
           return $str->fetchAll(PDO::FETCH_OBJ);
           }catch(Exception $e){
               die($e->getMessage()+"error en listarTipoMoneda()");
    
           } 
       }

       public function listarCuentaPropias(){
            $idT=$_POST['cuentaP'];
           // $id_cliente = "<script> document.write(cuentaP) </script>";
            $query = "SELECT nroCuenta FROM cuentapersonal WHERE clienteId = '$idT'";
            $str= $this->conexion->prepare($query);
            $str->execute();
            $respuesta= $str->fetchAll(PDO::FETCH_ASSOC);
            $count= $str->rowCount();

            //var_dump( $count);
            if($count>0){
                foreach ($respuesta as $lista)
                {
                echo "<option value='".$lista['nroCuenta']."'>".$lista['nroCuenta']." </option>";
                }
            }else{
                echo "<option value=''>No tienes registrada ninguna cuenta de origen</option>";
            }
        }
}
?>
<script>
    $(document).ready(function(){
        var cuentaP=localStorage.getItem('usuario_id');
    });
</script>