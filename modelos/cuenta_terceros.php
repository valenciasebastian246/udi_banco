<?php
  
class cuentaTercero{

    public $conexion;
    public $idTransaccion;
    public $monto_deposito;
    public $glosa;
    public $origenFondo;
    public $destinoFondo;
    public $fecha;
    public $estado;
    public $ClienteId;
    public $cuentaDebitoId;
    public $cuentaDepositoId;
    public $tipoMonedaId;

   public function __construct(){
       try{

        $this->conexion = Database::connect();

       }catch(Exception $e){
        die($e->getMessage()+" ERRROR_construct_cuenta_terceros");
       }
   }


   public function listarTipoMoneda(){
    try{
        $query = "SELECT * FROM tipoMoneda";
        $str= $this->conexion->prepare($query);
        $str->execute();
       return $str->fetchAll(PDO::FETCH_OBJ);
       }catch(Exception $e){
           die($e->getMessage()+"error en listarTipoMoneda()");

       } 
   }

   public function listarClientes(){
    try{
        $query = "SELECT c.idCliente,CONCAT(c.nombre,' ',c.ap_paterno,' ',c.ap_materno) as nombre_completo FROM cliente as c WHERE c.estado=0 ";
        $str= $this->conexion->prepare($query);
        $str->execute();
       return $str->fetchAll(PDO::FETCH_ASSOC);
       }catch(Exception $e){
           die($e->getMessage()+"error en listarClientes()");

       } 
   }

   public function listarClientesExceptoTitular($id){
    try{
    $query = "SELECT c.idCliente,CONCAT(c.nombre,' ',c.ap_paterno,' ',c.ap_materno) as nombre_completo FROM cliente as c 
    WHERE c.estado=0 and c.idCliente!=$id";
        $str= $this->conexion->prepare($query);
        $str->execute();
       return $str->fetchAll(PDO::FETCH_ASSOC);
       }catch(Exception $e){
           die($e->getMessage()+"error en listarClientesExceptoTitular()");

       } 
   }


   public function registrarTransaccion(cuentaTercero $data){
    try{
        $query = "INSERT into transaccion (monto_deposito,glosa,origenFondo,destinoFondo,ClienteId,
        cuentaDebitoId,cuentaDepositoId,tipoMonedaId)
        values(?,?,?,?,?,?,?,?)";
        //var_dump($data);
        if($data->monto_deposito!=null && $data->glosa!=null && $data->origenFondo !=null && $data->destinoFondo !=null && 
        $data->ClienteId !='selecciona un titular' && $data->cuentaDebitoId !='Selecciona la Cuenta de Débito' && 
        $data->cuentaDepositoId !='seleccione una cuenta de destino' && $data->tipoMonedaId!='Seleccione la moneda'){
           $resp= $this->conexion->prepare($query)->execute(
                array($data->monto_deposito,$data->glosa,$data->origenFondo,$data->destinoFondo,
                $data->ClienteId,$data->cuentaDebitoId,$data->cuentaDepositoId,$data->tipoMonedaId));
            

            return true;  
              
        }else{
            return false;
        } 
        
       }catch(Exception $e){
           die($e->getMessage()+"error en registrarTransaccion()");

       } 

       
   }

   //funcion no USADA XD
   public function ActualizacionDeSaldos($idCuentaP, $idCuentaD, $monto){
    try{
        //RESTA EL MONTO A LA CUENTA QUE HIZO LA TRANSFERENCIA
        $query = "UPDATE cuentapersonal as cp set cp.saldo = cp.saldo-'$monto' where cp.nroCuenta='$idCuentaP' ";
        $str= $this->conexion->prepare($query);
        $str->execute();
        
        //SUMA EL MONTO  A LA CUENTA TRANSFERIDA
        if($idCuentaD!=0){
            $queryS = "UPDATE cuentapersonal as cp, cuentadeposito as cd set cp.saldo = cp.saldo+'$monto' where cd.idCuentaDeposito='$idCuentaD' and cp.nroCuenta= cd.nroCuenta ";
            $strS= $this->conexion->prepare($queryS);
            $strS->execute();
        }
     

       }catch(Exception $e){
           die($e->getMessage()+"error en ActualizacionDeSaldos()");

       }  

   }

   public function listarUltimasTransacciones(){
    try{
        $query = "SELECT CONCAT(cp.nroCuenta,' ',tc.descripcion,' M/',tm.abreviacion) as origenCuenta, transaccion.monto_deposito,
                transaccion.glosa, transaccion.fecha,CONCAT(c.nombre,' ',c.ap_paterno,' ',c.ap_materno)as nombreCliente, 
                CONCAT(cd.nroCuenta,' ',tc.descripcion,' M/',tm.abreviacion) as CuentaDeposito, 
                tm.descripcion as tipoMoneda
                FROM transaccion INNER JOIN cuentapersonal as cp ON
                 transaccion.cuentaDebitoId=cp.nroCuenta
                INNER JOIN cliente as c ON transaccion.ClienteId=c.idCliente
                INNER JOIN tipocuenta as tc ON cp.tipoCuentaId=tc.id 
                INNER join tipomoneda as tm on transaccion.tipoMonedaId= tm.idTipoMoneda
                INNER JOIN cuentadeposito as cd on transaccion.cuentaDepositoId=cd.idCuentaDeposito
                where DATE_FORMAT(transaccion.fecha, '%Y-%m-%d') = CURDATE() and transaccion.estado=0";
        $str= $this->conexion->prepare($query);
        $str->execute();
        //$res=$str->fetchAll(PDO::FETCH_OBJ);
        //var_dump($res);
       return $str->fetchAll(PDO::FETCH_OBJ);
       }catch(Exception $e){
           die($e->getMessage()+"error en listarUltimasTransacciones()");

       }     
    }

}
