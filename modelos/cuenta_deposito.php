<?php
class cuenta_deposito{
    public $conexion;
    public $nroCuenta;
    public $fecha_registro;
    public $fecha_vencimiento;
    public $clienteId;
    public $tipoCuentaDepositoId;


    public function __construct(){
        try{
 
         $this->conexion = Database::connect();
 
        }catch(Exception $e){
         die($e->getMessage()+" ERRROR_construct_cuenta_terceros");
        }
    }

    
 public function verificarCuentaTerceroExistente(cuenta_deposito $data){

    try{
        
        $query = "SELECT  cd.nroCuenta as count_nro_cuenta FROM cuentadeposito as cd WHERE 
        cd.nroCuenta=$data->nroCuenta and cd.clienteId=$data->clienteId";
        $str= $this->conexion->prepare($query);
        $str->execute();
        //echo $query;
       $res=$str->fetchAll(PDO::FETCH_OBJ);
       $count= $str->rowCount();
       //var_dump($count);
        return $count;
    
       
       }catch(Exception $e){
           die($e->getMessage()+"error en verificarCuentaTerceroExistente()");

       }  
}


    public function registrarTipoCuentaTerceros(cuenta_deposito $data)
    {
        try {
            $query = "INSERT into cuentadeposito(nroCuenta,fecha_vencimiento,
            clienteId,tipoCuentaDepositoId)
            values(?,?,?,?)";
            // var_dump($data);
            if ($data->nroCuenta != null ) {
                if ($data->clienteId != null) {

                  
                    $resp = $this->conexion->prepare($query)->execute(
                        array($data->nroCuenta, $data->fecha_vencimiento, $data->clienteId, $data->tipoCuentaDepositoId,)
                    );
                } else {
                    return 2;
                }


                return 0;
            } else {
                return 1;
            }
        } catch (Exception $e) {
            die($e->getMessage() + "error en registrarTipoCuentaTerceros()");
        }
    }
    public function registrarTipoCuentaOtrosBancos(cuenta_deposito $data)
    {
        try {
            $query = "INSERT into cuentadeposito(nroCuenta,fecha_vencimiento,
            clienteId,tipoCuentaDepositoId)
            values(?,?,?,?)";
            // var_dump($data);
            if ($data->nroCuenta != null) 
            {
                if ($data->clienteId != null) 
                {
                    $resp = $this->conexion->prepare($query)->execute(
                        array($data->nroCuenta, $data->fecha_vencimiento, $data->clienteId, $data->tipoCuentaDepositoId,)
                    );
                    $rs =$this->conexion->lastInsertId();

                } else 
                {
                    return 1;
                }
                return $rs;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            die($e->getMessage() + "error en registrarTipoCuentaOtrosBancos()");
        }
    }
}

?>
